//
//  AppDelegate.swift
//  FNews
//
//  Created by Gabor Rusznyak on 16/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit
import Mixpanel

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var activePages = 0
    var newPosts = 0
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //Initialise Mixpanel
        Mixpanel.sharedInstanceWithToken("f663c7819e8622b8263ae85436ed1fdb")
        Mixpanel.sharedInstance().identify(Mixpanel.sharedInstance().distinctId)
        Mixpanel.sharedInstance().people.increment("App_Opened", by: 1);
        Mixpanel.sharedInstance().track("App_Opening")

        //Start SQLite thread
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            SQLiteHelper.instance.open("FNews.sqlite", inDocuments: true)
            //SQLiteHelper.instance.execute("alter table feeds add column fav INTEGER");
        })
        
        //Register for notifications
        application.registerUserNotificationSettings(
            UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil)
        )
        
        //Start Background Fetch
        let interval = Commons.instance.interval
        if (interval == 0.0) {
            application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        } else {
            application.setMinimumBackgroundFetchInterval(NSTimeInterval(interval * 3600))
        }
        
        return true
    }
    
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        let wifiOnly = NSUserDefaults.standardUserDefaults().boolForKey("SyncWifiOnly")
        let reachability = Reachability.reachabilityForInternetConnection()
        if !reachability!.isReachable() || (!reachability!.isReachableViaWiFi() && wifiOnly) {       
            completionHandler(UIBackgroundFetchResult.NoData)
            return
        }
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            let pages = SQLiteHelper.instance.query("select * from pages where active = 1")
            self.activePages = pages.count
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if (self.activePages > 0) {
                    self.newPosts = 0
                    FacebookLoader.instance.loadFeeds { (newPosts) -> Void in
                        self.newPosts += newPosts
                        self.activePages -= 1
                        if (self.activePages == 0) {
                            if (self.newPosts == 0) {
                                completionHandler(UIBackgroundFetchResult.NoData)
                            } else {
                                UIApplication.sharedApplication().applicationIconBadgeNumber += self.newPosts
                                completionHandler(UIBackgroundFetchResult.NewData)
                            }
                        }
                    }
                }
            })
        })
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        if let viewController = self.window?.rootViewController?.childViewControllers[0] as? ViewController {
            viewController.masterViewController?.reloadData()
        }
        
        // Analytics
        Mixpanel.sharedInstance().track("App active")

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

