//
//  Commons.swift
//  FNews
//
//  Created by Gabor Rusznyak on 27/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation
import StoreKit

class Commons: NSObject {

    class var instance : Commons {
        struct Static {
            static let instance : Commons = Commons()
        }
        return Static.instance
    }
    
    /**************/
    let demo = true
    /**************/
    
    var selectedPostId = 0
    var products: Array<SKProduct>?

    var intervals = Array<AnyObject>()
    var reserveIntervals = Array<AnyObject>()
    
    var sqlQueue = dispatch_queue_create("com.swguru.fnews.sqlite", nil)

    var selectedInterval: Int {
        get {
            return NSUserDefaults.standardUserDefaults().integerForKey("SelectedInterval")
        }
        set(interval) {
            NSUserDefaults.standardUserDefaults().setInteger(interval, forKey: "SelectedInterval")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var reserveInterval: Int {
        get {
            return NSUserDefaults.standardUserDefaults().integerForKey("ReserveInterval")
        }
        set(interval) {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "ReserveSet")
            NSUserDefaults.standardUserDefaults().setInteger(interval, forKey: "ReserveInterval")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var interval: Double {
        let dict: AnyObject = intervals[selectedInterval]
        return dict["time"] as! Double
    }
    
    var images = Dictionary<String, NSData>()
    
    override init() {
        super.init()
        //self.selectedInterval = 0
        intervals.append(["name": String.localised("AUTO"), "time": 0.00])
        intervals.append(["name": "15 " + String.localised("MINS"), "time": 0.25])
        intervals.append(["name": "30 " + String.localised("MINS"), "time": 0.5])
        intervals.append(["name": "1 " + String.localised("HOUR"), "time": 1.0])
        intervals.append(["name": "2 " + String.localised("HOURS"), "time": 2.0])
        intervals.append(["name": "4 " + String.localised("HOURS"), "time": 4.0])
        intervals.append(["name": "6 " + String.localised("HOURS"), "time": 6.0])
        intervals.append(["name": "12 " + String.localised("HOURS"), "time": 12.0])
        intervals.append(["name": "24 " + String.localised("HOURS"), "time": 24.0])

        reserveIntervals.append(["name": "1 " + String.localised("DAY"), "time": 1.00])
        reserveIntervals.append(["name": "2 " + String.localised("DAYS"), "time": 2.00])
        reserveIntervals.append(["name": "3 " + String.localised("DAYS"), "time": 3.00])
        reserveIntervals.append(["name": "4 " + String.localised("DAYS"), "time": 4.00])
        reserveIntervals.append(["name": "1 " + String.localised("WEEK"), "time": 7.00])
        reserveIntervals.append(["name": "2 " + String.localised("WEEKS"), "time": 14.00])
        reserveIntervals.append(["name": "3 " + String.localised("WEEKS"), "time": 21.00])
        reserveIntervals.append(["name": "1 " + String.localised("MONTH"), "time": 30.00])
        
        if (!NSUserDefaults.standardUserDefaults().boolForKey("ReserveSet")) {
            reserveInterval = 4
        }

    }
    
    
}
