//
//  CustomActivity.swift
//  CustomActivity
//
//  Created by Gabor Rusznyak on 21/01/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import UIKit

class CustomActivity: UIActivity {
   
    override func activityType() -> String? {
        return "addfav.swguru.com";
    }
    
    override func activityTitle() -> String? {
        return "Add to Favourites";
    }
    
    override func activityImage() -> UIImage? {
        if (UIDevice.currentDevice().userInterfaceIdiom == .Phone) {
            return UIImage(named: "AddFaviPhone")
        } else {
            return UIImage(named: "AddFaviPad")
        }
    }
    
    override func canPerformWithActivityItems(activityItems: [AnyObject]) -> Bool {
        println(__FUNCTION__)
        return true;
    }
    
    override func prepareWithActivityItems(activityItems: [AnyObject]) {
        println(__FUNCTION__)
    }
    
    override func activityViewController() -> UIViewController? {
        println(__FUNCTION__)
        return nil
    }
    
    override func performActivity() {
        println("Set Favourite: \(Commons.instance.selectedPostId)")
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            let rowId = SQLiteHelper.instance.execute("update feeds set fav = 1 where id = \(Commons.instance.selectedPostId)")
        })

        activityDidFinish(true)
    }
    
}
