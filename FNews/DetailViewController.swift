//
//  DetailViewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 16/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit


protocol DetailViewControllerDelegate {
    func detailViewControllerFavChanged(detailViewController: DetailViewController)
}

class DetailViewController: UIViewController, UIWebViewDelegate, UIActivityItemSource {

    @IBOutlet var webView: UIWebView?
    @IBOutlet var readableButton: UIBarButtonItem?
    @IBOutlet var backButton: UIBarButtonItem?
    @IBOutlet var shareButton: UIBarButtonItem?
    @IBOutlet var favButton: UIBarButtonItem?
    @IBOutlet var indyView: UIActivityIndicatorView?
    
    let numberOfSharedItems = 6
    var itemNo = 0

    var readable = false
    var baseLink = ""
    var link = ""
    var picture = ""
    var webTitle = ""
    var readableDict: AnyObject?
    var showIndy = false
    var isFav = false
    
    var delegate: DetailViewControllerDelegate?

    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            if let detail = self.detailItem as? Dictionary<String, AnyObject>  {
                if (detail["link"] != nil) {
                    link = detail["link"]! as! String
                }
                if (detail["picture"] != nil) {
                    picture = detail["picture"]! as! String
                }
                if (detail["id"] != nil) {
                    if let pidStr = detail["id"] as? String {
                        let pid = Int(pidStr)
                        Commons.instance.selectedPostId = pid!
                    }
                }
                isFav = false
                if (detail["fav"] != nil) {
                    if let fav = detail["fav"]! as? String {
                        isFav = (fav == "1")
                    }
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Header"), forBarMetrics: UIBarMetrics.Default)
        navigationController?.toolbar.setBackgroundImage(UIImage(named: "Header"), forToolbarPosition: UIBarPosition.Any, barMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.toolbar.tintColor = UIColor.whiteColor()
        backButton?.enabled = webView!.canGoBack
        shareButton?.enabled = false
        favButton?.enabled = false
        readableButton?.enabled = false
        
        favButton?.image = UIImage(named: (isFav ? "RemoveFav" : "AddFav"))
        
        if (!link.isEmpty) {
            configureView()
        } else {
            webView?.loadHTMLString("<html></html>", baseURL: NSURL(string: "http://localhost"))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        webView?.stopLoading()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back(sender: UIButton) {
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        navigationController?.popViewControllerAnimated(true)
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        showIndy = true
        if (readable) {
            loadReadability()
        } else {
            let url = NSURL(string: link)!
            let request = NSURLRequest(URL: url)
            webView?.loadRequest(request)
        }
    }

    func loadReadability() {
        webView?.stopLoading()
        var size = "large"
        if (isPhone) {
            size = "medium"
        }
        let path = NSBundle.mainBundle().pathForResource("readability", ofType: "js")
        let javascript = "readSize='size-\(size)';" + (try! String(contentsOfFile: path!, encoding: NSUTF8StringEncoding))
        webView?.stringByEvaluatingJavaScriptFromString(javascript)
        
        // Analytics
    }
    
    //MARK: - IBActions
    
    @IBAction func goBack(sender: UIBarButtonItem) {
        webView?.goBack()
    }
    
    @IBAction func share(sender: UIBarButtonItem) {
        if (detailItem == nil) {
            return
        }

        let items = [self, self, self, self, self, self]
        let shareController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        shareController.setValue(webTitle, forKey: "subject")
        if (!isPhone) {
            if let buttonView = sender.valueForKey("view") as? UIView {
                shareController.popoverPresentationController?.sourceView = buttonView
            } else {
                shareController.popoverPresentationController?.sourceView = navigationController?.toolbar
            }
        }
        self.mainViewController!.presentViewController(shareController, animated: true, completion: nil)
    }
    
    @IBAction func switchReadable(sender: UIBarButtonItem) {
        readable = !readable
        sender.image = UIImage(named: "Readable" + (readable ? "Selected" : ""))
        configureView()
    }
    
    @IBAction func changeFav(sender: UIBarButtonItem) {
        print("Set Favourite: \(Commons.instance.selectedPostId)")
        isFav = !isFav
        favButton?.image = UIImage(named: (isFav ? "RemoveFav" : "AddFav"))
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            SQLiteHelper.instance.execute("update feeds set fav = \(self.isFav ? 1 : 0) where id = \(Commons.instance.selectedPostId)")
        })
        alert(String.localised("FAV"), message: String.localised(isFav ? "ADDED_FAV" : "REMOVED_FAV"))
        if (!isPhone) {
            self.delegate?.detailViewControllerFavChanged(self)
        }
    }
    
    //MARK: - UIActivity Item Source
    
    func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> AnyObject {
        var data: NSData?
        if (self.detailItem!["image"] != nil) {
            if let image = self.detailItem!["image"] as? UIImage {
                data = UIImageJPEGRepresentation(image, 1.0)
            }
        }
        
        if (itemNo < numberOfSharedItems - 1) {
            itemNo += 1
        } else {
            itemNo = 0
        }

        switch (itemNo) {
        case 1:
            return webTitle
        case 2:
            return (data != nil) ? data! : ""
        case 3:
            return link
        case 4:
            return ""
        case 5:
            return String.localised("SHARED_BY")
        case 0:
            return "http://facynews.com"
        default:
            return "";
        }
    }
    
    func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject? {
        var data: NSData?
        if (self.detailItem!["image"] != nil) {
            if let image = self.detailItem!["image"] as? UIImage {
                data = UIImageJPEGRepresentation(image, 1.0)
            }
        }
        
        if (itemNo < numberOfSharedItems - 1) {
            itemNo += 1
        } else {
            itemNo = 0
        }
        
        if (activityType == UIActivityTypePostToTwitter) {
            switch (itemNo) {
            case 1:
                return webTitle + " " + link
            case 2:
                return String.localised("SHARED_BY") + " " + "http://facynews.com"
            default:
                return nil
            }
        } else if (activityType == UIActivityTypePostToFacebook) {
            switch (itemNo) {
            case 1:
                return link
            default:
                return nil
            }
        } else if (activityType == UIActivityTypeMail) {
            switch (itemNo) {
            case 1:
                return link
            case 2:
                return ""
            case 3:
                return String.localised("SHARED_BY")
            case 4:
                return "http://facynews.com"
            default:
                return nil;
            }
        } else {
            switch (itemNo) {
            case 1:
                return webTitle
            case 2:
                return (data != nil) ? data! : ""
            case 3:
                return link
            case 4:
                return ""
            case 5:
                return String.localised("SHARED_BY")
            case 0:
                return "http://facynews.com"
            default:
                return nil;
            }
        }
    }
    
    //MARK: - Webview delegate
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if (navigationType == UIWebViewNavigationType.LinkClicked) {
            readable = false
            readableButton?.image = UIImage(named: "Readable" + (readable ? "Selected" : ""))
            readableDict = nil
            link = request.URL!.description
        }
        if (showIndy) {
            showIndy = false
            backButton?.enabled = false
            indyView?.startAnimating()
            shareButton?.enabled = false
            favButton?.enabled = false
            readableButton?.enabled = false
        }
        return true
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        if (!webView.loading) {
            indyView?.stopAnimating()
            if (detailItem != nil) {
                shareButton?.enabled = true
                favButton?.enabled = true
                readableButton?.enabled = true
                backButton?.enabled = webView.canGoBack
            }
        }
        webTitle = webView.stringByEvaluatingJavaScriptFromString("document.title") as String!
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print(error.debugDescription)
        if (!webView.loading) {
            indyView?.stopAnimating()
            if (detailItem != nil) {
                shareButton?.enabled = true
                favButton?.enabled = true
                readableButton?.enabled = true
                backButton?.enabled = webView.canGoBack
            }
        }
    }
}

