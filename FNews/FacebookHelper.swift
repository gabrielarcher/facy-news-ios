//
//  FacebookHelper.swift
//  FNews
//
//  Created by Gabor Rusznyak on 16/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation
import Accounts
import Social
import UIKit

protocol FacebookHelperDelegate {
    func facebookLoginSuccess(facebookHelper: FacebookHelper)
    func facebookLoginError(facebookHelper: FacebookHelper, showMessage: Bool)
    func facebookLoginRenew(facebookHelper: FacebookHelper)
    func facebookGetSuccess(facebookHelper: FacebookHelper, method: String, response: AnyObject)
    func facebookBackgroundSuccess(facebookHelper: FacebookHelper, method: String, response: AnyObject)
}

class FacebookHelper {

    var accountStore = ACAccountStore()
    var account: ACAccount?
    
    var delegate: FacebookHelperDelegate?
    var shouldAlert = false
    
    class var instance : FacebookHelper {
        struct Static {
            static let instance : FacebookHelper = FacebookHelper()
        }
        return Static.instance
    }

    func login() {
        let accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
        let key = NSBundle.mainBundle().objectForInfoDictionaryKey("FacebookAppId") as! String
        let options = [ACFacebookAppIdKey: key, ACFacebookPermissionsKey: ["public_profile", "user_likes"]]
        accountStore.requestAccessToAccountsWithType(accountType, options: options as [NSObject : AnyObject]) { (granted, error) -> Void in
            if (error != nil) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if (self.delegate != nil) {
                        self.delegate?.facebookLoginError(self, showMessage: true)
                    }
                })
            } else {
                if (granted) {
                    var accounts = self.accountStore.accountsWithAccountType(accountType) as! Array<ACAccount>
                    print(accounts)
                    self.account = accounts[0]
                    self.accountStore.renewCredentialsForAccount(self.account, completion: { (result, error) -> Void in
                        if (error == nil) {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                if (self.delegate != nil) {
                                    self.delegate!.facebookLoginSuccess(self)
                                }
                            })
                        } else {
                            print("RENEW ERROR")
                        }
                    })
                } else {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        if (self.delegate != nil) {
                            self.delegate?.facebookLoginError(self, showMessage: true)
                        }
                    })
                }
            }
        }
    }

    func getGraph(urlStr: String) {
        getGraph(urlStr, parameters: Dictionary<String, String>())
    }
    
    func getGraph(urlStr: String, parameters: Dictionary<String, String>) {
        let request = SLRequest(forServiceType: SLServiceTypeFacebook, requestMethod: SLRequestMethod.GET,
            URL: NSURL(string: "https://graph.facebook.com/\(urlStr)"), parameters: parameters)
        
        request.account = self.account
        request.performRequestWithHandler { (data, urlResponse, error) -> Void in
            if (error == nil) {
                do {
                    let response = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as! Dictionary<String, AnyObject>
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        if (self.shouldAlert) {
                            self.delegate?.facebookGetSuccess(self, method: urlStr, response: response)
                        } else {
                            self.delegate?.facebookBackgroundSuccess(self, method: urlStr, response: response)
                        }
                    })
                } catch {
                    if (self.shouldAlert) {
                        
                    } else {
                        self.accountStore.renewCredentialsForAccount(self.account, completion: { (result, error) -> Void in
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                if (self.delegate != nil) {
                                    self.delegate!.facebookLoginRenew(self)
                                }
                            })
                        })
                    }
                }
            }
        }
    
    }
    
}