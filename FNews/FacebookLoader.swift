//
//  FacebookLoader.swift
//  FNews
//
//  Created by Gabor Rusznyak on 20/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit
import Mixpanel
import Social

class FacebookLoader: NSObject, FacebookHelperDelegate {
    
    var feedCounter = 0
    var feeds = []
    var pages = []
    var loggedIn = false
    var getPages = true
    var isBackground = false
    var completionHandler: (newPosts: Int) -> Void = {
        (newPosts: Int) -> Void in
    }

    class var instance : FacebookLoader {
        struct Static {
            static let instance : FacebookLoader = FacebookLoader()
        }
        return Static.instance
    }
    

    func loadPages() {
        FacebookHelper.instance.delegate = self
        FacebookHelper.instance.shouldAlert = true
        if (!loggedIn) {
            getPages = true
            FacebookHelper.instance.login()
        } else {
            dispatch_async(Commons.instance.sqlQueue, { () -> Void in
                SQLiteHelper.instance.execute("update pages set deleted = 1 where id > 0 and page_source = 1")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    FacebookHelper.instance.getGraph("me/likes")
                })
            })
        }
    }
    
    func loadFeeds(completionHandler: (newPosts: Int) -> Void) {
        self.completionHandler = completionHandler
        loadFBFeeds(true)
    }

    func loadFeeds() {
        loadFBFeeds(false)
    }
    
    func loadFBFeeds(isBackground: Bool) {
        FacebookHelper.instance.delegate = self
        FacebookHelper.instance.shouldAlert = !isBackground
        self.isBackground = isBackground
        if (!loggedIn) {
            getPages = false
            FacebookHelper.instance.login()
        } else {
            dispatch_async(Commons.instance.sqlQueue, { () -> Void in
                self.pages = SQLiteHelper.instance.query("select * from pages where active = 1 order by time desc")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if (self.pages.count == 0) {
                        NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookError",
                            object: String.localised("FB_DONT_HAVE"))
                        return
                    }
                    self.feeds = []
                    
                    for page in self.pages {
                        let pageId: String = page["page_id"] as! String
                        FacebookHelper.instance.getGraph("\(pageId)/feed")
                    }
                    
                    if !NSUserDefaults.standardUserDefaults().boolForKey("MixpanelRegistered") {
                        self.getBasicData()
                    }
                })
            })
        }
        
    }

    func facebookLoginSuccess(facebookHelper: FacebookHelper) {
        loggedIn = true
        if (getPages) {
            loadPages()
        } else {
            loadFBFeeds(isBackground)
        }
        if (!NSUserDefaults.standardUserDefaults().boolForKey("LoggedWithFacebook")) {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "LoggedWithFacebook")
            NSUserDefaults.standardUserDefaults().synchronize()
            NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookLogged", object: nil)
        }
        
        getBasicData()
    }
    
    func getBasicData() {
        let request = SLRequest(forServiceType: SLServiceTypeFacebook, requestMethod: SLRequestMethod.GET,
                                URL: NSURL(string: "https://graph.facebook.com/me"), parameters: nil)
        
        request.account = FacebookHelper.instance.account
        request.performRequestWithHandler { (data, urlResponse, error) -> Void in
            if (error == nil) {
                do {
                    let response = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as! Dictionary<String, AnyObject>
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        print(response)
                        let uid = response["id"] as? String ?? ""
                        let name = response["name"] as? String ?? ""
                        let email = response["email"] as? String ?? ""
                        let gender = response["gender"] as? String ?? ""
                        let birthday = response["birthday"] as? String ?? ""
                        Mixpanel.sharedInstance().people.set([
                            "$distinct_id": uid,
                            "$email": email,
                            "$name": name,
                            "Gender": gender,
                            "Birthday": birthday
                            ]);
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "MixpanelRegistered")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    })
                } catch let error as NSError {
                    print(error)
                }
            }
        }
    }
    
    func facebookLoginError(facebookHelper: FacebookHelper, showMessage: Bool) {
        if (showMessage) {
            UIAlertView(title: "Facebook",
                message: String.localised("FB_NOT_SET"), delegate: nil, cancelButtonTitle: "OK").show()
        }
        NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookLoginError", object: nil)
    }
    
    func facebookLoginRenew(facebookHelper: FacebookHelper) {
        UIAlertView(title: "Facebook",
            message: String.localised("FB_RENEW"), delegate: nil, cancelButtonTitle: "OK").show()
        NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookLoginError", object: nil)
    }

    func facebookBackgroundSuccess(facebookHelper: FacebookHelper, method: String, response: AnyObject) {
        self.isBackground = true
        facebookGetSuccess(facebookHelper, method: method, response: response)
    }

    func facebookGetSuccess(facebookHelper: FacebookHelper, method: String, response: AnyObject) {
        if (method.contains("me/likes")) {
            //Check if page is in database already
            dispatch_async(Commons.instance.sqlQueue, { () -> Void in
                for page in response["data"] as! [AnyObject] {
                    let pageId = page["id"] as! String
                    let category = page["category"] as! String
                    var name = page["name"] as! String
                    name = name.replace("\"", with: "'")
                    let time = page["created_time"] as! String?
                    var date = NSDate()
                    if (time != nil) {
                        date = NSDate(dateString: time!)
                    }
                    let result = SQLiteHelper.instance.query("select page_name from pages where page_id=\(pageId) and page_source = 1")
                    if (result.count == 0) {
                        SQLiteHelper.instance.execute("insert into pages (page_id, category, page_name, active, page_source, time) values (\"\(pageId)\", \"\(category)\", \"\(name)\", 0, 1, \(date.timestamp))")
                    } else {
                        SQLiteHelper.instance.execute("update pages set deleted = 0, page_name = \"\(name)\" where page_id =\"\(pageId)\"")
                    }
                }
                
                //Paging if needed
                if var paging = response["paging"] as? Dictionary<String, AnyObject> {
                    if (paging.indexForKey("next") != nil) {
                        let cursors: AnyObject! = paging["cursors"] as AnyObject!
                        let after: String = cursors["after"] as! String
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            FacebookHelper.instance.getGraph("me/likes", parameters: ["after": after])
                        })
                    } else {
                        SQLiteHelper.instance.execute("delete from pages where deleted = 1 and page_source = 1")
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookPagesLoaded", object: nil)
                        })
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookPagesLoaded", object: nil)
                    })
                }
            })
            
        } else if (method.contains("/feed")) {
            let resp: Dictionary<String, AnyObject> = response as! Dictionary
            if (resp.indexForKey("error") == nil) {
                //process news
                var pageName = ""
                let pageId = method.replace("/feed", with: "")
                dispatch_async(Commons.instance.sqlQueue, { () -> Void in

                    var newPosts = 0
                    let interval = Int(Commons.instance.reserveIntervals[Commons.instance.reserveInterval]["time"] as! Float)
                    let deleteTime = NSDate().timestamp - (interval * 86400)
                    
                    let result = SQLiteHelper.instance.query("select page_name from pages where page_id = '\(pageId)'")
                    if (result.count > 0) {
                        pageName = result[0]["page_name"] as String!
                    }
                    for post in resp["data"] as! [AnyObject] {
                        let post_id = post["id"] as! String
                        let type = post["type"] as! String
                        if (type == "link") {
                            
                            let from = post["from"] as! [String: AnyObject]?
                            let from_name = String.defaultValue(from?["name"] as? String)
                            if (from_name == pageName) {
                                let caption = String.defaultValue(post["caption"] as? String)
                                let link = String.defaultValue(post["link"] as? String)
                                let name = String.defaultValue(post["name"] as? String).replace("\"", with: "'")
                                
                                //Picture
                                var picture = post["picture"] as? String ?? ""
                                var p = picture.indexOf("url=") + 4
                                if (p > 4) {
                                    picture = picture.substringFrom(p)
                                    p = picture.indexOf("&")
                                    if (p > 1) {
                                        picture = picture.substringTo(p)
                                    }
                                }
                                picture = picture.urlDecode()
                                
                                p = picture.indexOf("url=") + 4
                                if (p > 4) {
                                    picture = picture.substringFrom(p)
                                    p = picture.indexOf("&")
                                    if (p > 1) {
                                        picture = picture.substringTo(p)
                                    }
                                }
                                picture = picture.urlDecode()
                                if name.contains("MIT") {
                                    print(picture)
                                }
                                
                                let time = post["created_time"] as? String
                                var date = NSDate()
                                if (time != nil) {
                                    date = NSDate(dateString: time!)
                                }
                                
                                if (date.timestamp > deleteTime) {
                                    let result = SQLiteHelper.instance.query("select * from feeds where post_id = \"\(post_id)\"")
                                    if (result.count == 0) {
                                        
                                        //save post
                                        SQLiteHelper.instance.execute("insert into feeds (post_id, page, time, caption, author, " +
                                            "link, name, picture, feed_type, read, fav) " +
                                            "values (\"\(post_id)\", \"\(pageId)\", \"\(date.timestamp)\", \"\(caption)\", " +
                                            "\"\(from_name)\", \"\(link)\", \"\(name)\", \"\(picture)\", \"\(type)\", 0, 0)")
                                        newPosts += 1
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        //Call back to refresh
                        if (self.isBackground) {
                            self.completionHandler(newPosts: newPosts)
                        } else {
                            NSNotificationCenter.defaultCenter().postNotificationName("NotificationFacebookFeedLoaded", object: nil)
                        }
                        
                    })
                })
            }
            

        }
    }
    
}
