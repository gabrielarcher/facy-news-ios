//
//  IntervalViewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 02/12/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

class IntervalViewController: UITableViewController {
    
    var isReserve = false

    override func viewDidLoad() {
        super.viewDidLoad()

        setCustomBackButton("BackBtn")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.toolbar.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isReserve) {
            return Commons.instance.reserveIntervals.count
        } else {
            return Commons.instance.intervals.count
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 

        if (isReserve) {
            cell.textLabel?.text = Commons.instance.reserveIntervals[indexPath.row]["name"] as? String
        } else {
            cell.textLabel?.text = Commons.instance.intervals[indexPath.row]["name"] as? String
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (isReserve) {
            Commons.instance.reserveInterval = indexPath.row
        } else {
            Commons.instance.selectedInterval = indexPath.row
            let interval = Commons.instance.interval
            if (interval == 0.0) {
                UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
            } else {
                UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(NSTimeInterval(interval * 3600))
            }
        }
        if let settingsController = self.presentingViewController as? SettingsViewController {
           settingsController.tableView?.reloadData()
        }
        navigationController?.popViewControllerAnimated(true)
    }

}
