//
//  LoginController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 20/02/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loggedInWithFacebook(_:)), name:"NotificationFacebookLogged", object: nil)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        if (isPhone) {
            navigationController?.toolbar.hidden = true
        }
        if (NSUserDefaults.standardUserDefaults().boolForKey("LoggedWithFacebook")) {
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: UIButton) {
        mainViewController?.masterViewController?.startLogin()
    }

    //Notification
    
    func loggedInWithFacebook(notification: NSNotification) {
        if let pagesController = storyboard?.instantiateViewControllerWithIdentifier("Pages") as? PagesViewController {
            pagesController.siteId = 1
            navigationController?.pushViewController(pagesController, animated: true)
        }
    }

}
