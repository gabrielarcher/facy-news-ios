//
//  MasterCell.swift
//  FNews
//
//  Created by Gabor Rusznyak on 20/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

class MasterCell: UICollectionViewCell {

    @IBOutlet var pageLabel: UILabel?
    @IBOutlet var captionLabel: UILabel?
    @IBOutlet var dateLabel: UILabel?
    @IBOutlet var timeLabel: UILabel?
    @IBOutlet var authorLabel: UILabel?
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var logoView: UIImageView?
    @IBOutlet var pictureView: UIImageView?
    @IBOutlet var backView: UIView?
    @IBOutlet var readView: UIImageView?

    var pictureLink = ""
    var logoLink = ""
}
