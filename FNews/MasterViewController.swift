//
//  MasterViewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 16/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit
import Kingfisher
import Mixpanel

class MasterViewController: UIViewController, DetailViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    @IBOutlet var collectionView: UICollectionView?
    @IBOutlet var organiseButton: UIButton?

    var detailViewController: DetailViewController? = nil
    var activePages = 0
    var pageId = ""
    var pageName = ""

    let facebookLoader = FacebookLoader()
    var feeds = Array<AnyObject>()
    var refreshView = UIRefreshControl()
    var isViewed = false
    var isFav = false
    var searchText = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(facebookPagesLoaded(_:)), name:"NotificationFacebookPagesLoaded", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(facebookFeedLoaded(_:)), name:"NotificationFacebookFeedLoaded", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(facebookLoadError(_:)), name:"NotificationFacebookLoadError", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(facebookLoginError(_:)), name:"NotificationFacebookLoginError", object: nil)

        setNeedsStatusBarAppearanceUpdate()
        if (pageId.isEmpty) {
            navigationItem.title = "Facy News"
        } else {
            navigationItem.title = pageName
            setCustomBackButton("BackBtn")
        }
        
        let cache = KingfisherManager.sharedManager.cache
        cache.maxDiskCacheSize = 30 * 1024 * 1024
        cache.maxCachePeriodInSecond = 3600 * 24 * 3

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        //Pull to refresh
        refreshView.attributedTitle = NSAttributedString(string: String.localised("REFRESHING_POSTS"))
        refreshView.addTarget(self, action:#selector(refresh(_:)), forControlEvents: .ValueChanged)
        collectionView?.addSubview(refreshView)
        collectionView?.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Header"), forBarMetrics: UIBarMetrics.Default)
        navigationController?.toolbar.setBackgroundImage(UIImage(named: "Header"), forToolbarPosition: UIBarPosition.Any, barMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.toolbar.tintColor = UIColor.whiteColor()
        if (isPhone) {
            navigationController?.toolbar.hidden = false
        }
        if (!NSUserDefaults.standardUserDefaults().boolForKey("LoggedWithFacebook")) {
            if let loginController = storyboard?.instantiateViewControllerWithIdentifier("Login") as? LoginController {
                navigationController?.pushViewController(loginController, animated: false)
            }
        } else {
            reloadData()
        }

    }
    
    override func didReceiveMemoryWarning() {
        KingfisherManager.sharedManager.cache.clearMemoryCache()
    }
    
    func startLogin() {
        mainViewController?.loaderView?.hidden = false
        facebookLoader.loadPages()
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
         collectionView?.reloadData()
    }
    
    func refresh(sender: UIRefreshControl) {
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            let pages = SQLiteHelper.instance.query("select * from pages where active = 1")
            self.activePages = pages.count
            if (self.activePages > 0) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.refreshView.attributedTitle = NSAttributedString(string: String.localised("REFRESHING"))
                    self.refreshView.beginRefreshing()
                    self.facebookLoader.loadFeeds()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.refreshView.attributedTitle = NSAttributedString(string: String.localised("REFRESHING_POSTS"))
                    self.refreshView.endRefreshing()
                })
            }
        })
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    func reloadData() {
        var whereStr = ""
        var limit = ""
        if (!pageId.isEmpty) {
            whereStr = " where pages.page_id = \(pageId) "
        }
        if (!NSUserDefaults.standardUserDefaults().boolForKey("ShowReadPosts")) {
            if (whereStr.isEmpty) {
                whereStr = " where read = 0 "
            } else {
                whereStr += "and read = 0 "
            }
        }
        if (isFav) {
            if (whereStr.isEmpty) {
                whereStr = " where fav = 1 "
            } else {
                whereStr += "and fav = 1 "
            }
        }
        if (searchText.isEmpty) {
            if (whereStr.isEmpty) {
                whereStr = " where feeds.time > \(NSDate().timestamp - 86400) "
            } else {
                whereStr += "and feeds.time > \(NSDate().timestamp - 86400) "
            }
        } else {
            if (whereStr.isEmpty) {
                whereStr = " where lower(feeds.name) like \"%\(searchText.lowercaseString)%\" "
            } else {
                whereStr += "and lower(feeds.name) like \"%\(searchText.lowercaseString)%\" "
            }
            limit = " limit 100"
        }
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            
            print("select feeds.*, pages.page_id, pages.page_name from feeds " +
                "left join pages on feeds.page = pages.page_id \(whereStr) order by feeds.time desc\(limit)")
            
            self.feeds = SQLiteHelper.instance.query("select feeds.*, pages.page_id, pages.page_name from feeds " +
                "left join pages on feeds.page = pages.page_id \(whereStr) order by feeds.time desc\(limit)")
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.collectionView!.reloadData()
                self.refreshView.attributedTitle = NSAttributedString(string: String.localised("PULL_TO_REFRESH"))
                self.refreshView.endRefreshing()
            })
        })
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchText = searchBar.text ?? ""
        searchBar.resignFirstResponder()
        reloadData()
    }
    
    func startFacebookFeed() {
        //Delete older posts
        let interval = Int(Commons.instance.reserveIntervals[Commons.instance.reserveInterval]["time"] as! Float)
        let time = NSDate().timestamp - (interval * 86400)
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            SQLiteHelper.instance.execute("delete from feeds where time < \(time) and fav != 1")
        })

        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            let result = SQLiteHelper.instance.query("select id from pages where active = 1")
            if (result.count > 0) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.collectionView?.setContentOffset(CGPointMake(0, -140), animated: true)
                    self.refreshView.attributedTitle = NSAttributedString(string: String.localised("REFRESHING_POSTS"))
                    self.refreshView.beginRefreshing()
                    self.refresh(self.refreshView)
                })
            }
        })
    }
    
    
    //MARK: - IBActions
    
    @IBAction func settings(sender: AnyObject) {
        if let settingsController = storyboard?.instantiateViewControllerWithIdentifier("Settings") as? SettingsViewController {
            if (isPhone) {
                navigationController?.pushViewController(settingsController, animated: true)
            } else {
                print(self.mainViewController)
                self.mainViewController?.navigationController?.pushViewController(settingsController, animated: true)
            }
        }
    }
    
    @IBAction func favs(sender: UIBarButtonItem) {
        isFav = !isFav
        sender.image = UIImage(named: "Favs" + (isFav ? "Selected" : ""))
        reloadData()
    }
    
    func back(sender: UIButton) {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK: - Facebook Loader Delegate
    
    func facebookPagesLoaded(notification: NSNotification) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.mainViewController!.loaderView!.alpha = 0
        })
        if (navigationController?.topViewController != self) {
            return
        }
    }
    
    func facebookFeedLoaded(notification: NSNotification) {
        activePages -= 1
        if (activePages == 0) {
            reloadData()
        }
    }
    
    func facebookLoadError(notification: NSNotification) {
        if (navigationController?.topViewController != self) {
            return
        }
        if (refreshView.refreshing) {
            refreshView.endRefreshing()
        }
        refreshView.attributedTitle = NSAttributedString(string: String.localised("PULL_TO_REFRESH"))
    }
   
    func facebookLoginError(notification: NSNotification) {
        mainViewController?.loaderView?.hidden = true
        self.refreshView.endRefreshing()
    }
    
    //MARK: - Collection View Delegate
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let view: UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "SearchHeader", forIndexPath: indexPath) as UICollectionReusableView
        return view
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: MasterCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! MasterCell
        
        let feed = feeds[indexPath.item] as! Dictionary<String, AnyObject>
        var read = false
        if let iread = feed["read"] as? String {
            if (iread == "1") {
                read = true
            }
        }
        var fav = false
        if let ifav = feed["fav"] as? String {
            if (ifav == "1") {
                fav = true
            }
        }
        cell.pageLabel?.text = feed["page_name"] as? String
        cell.pageLabel?.alpha = read ? 0.5 : 1
        cell.captionLabel?.text = feed["caption"] as? String
        cell.captionLabel?.alpha = read ? 0.5 : 1
        cell.nameLabel?.text = feed["name"] as? String
        cell.nameLabel?.alpha = read ? 0.5 : 1

        cell.backView?.frame = cell.bounds

        if let timeStr =  feed["time"] as? String {
            if let time = Int(timeStr) {
                let date = NSDate(timestamp: time)
                cell.dateLabel?.text = date.longDate()
                cell.dateLabel?.alpha = read ? 0.5 : 1
                cell.timeLabel?.text = date.longTime()
                cell.timeLabel?.alpha = read ? 0.5 : 1
            }
        }
        
        //Download image
        let urlStr = feed["picture"] as! String
        if let url = NSURL(string: urlStr) {
            cell.pictureView!.kf_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder"), optionsInfo: [.Transition(ImageTransition.Fade(1))])
        }
        let pageIdStr = feed["page_id"] as? String ?? ""
        let urlStr2 = "https://graph.facebook.com/\(pageIdStr)/picture?type=small"
        if let url2 = NSURL(string: urlStr2) {
            cell.logoView!.kf_setImageWithURL(url2, placeholderImage: UIImage(named: "placeholder"), optionsInfo: [.Transition(ImageTransition.Fade(1))])
        }
        
        
        cell.readView?.hidden = !read
        cell.readView?.image = UIImage(named: fav ? "InFav" : "Tick")
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
            var width = collectionView.width - 8
            if (isPhone) {
                if (isPortrait) {
                    width = collectionView.width - 8
                } else if (self.view.height < 380 && self.view.width > 480) {
                    width = (collectionView.width - 8) / 2
                } else {
                    width = collectionView.width - 8
                }
            }
            return CGSizeMake(width, 250)
    }

    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.collectionView?.indexPathsForSelectedItems() {
                var object = feeds[indexPath[0].row] as! Dictionary<String, AnyObject>
                print(object)
                let cell = sender as! MasterCell
                object["image"] = cell.pictureView!.image
                if let pidStr = object["id"] as? String {
                    let pid = Int(pidStr)
                    dispatch_async(Commons.instance.sqlQueue, { () -> Void in
                        SQLiteHelper.instance.execute("update feeds set read = 1 where id = \(pid!)")
                    })
                    var index = -1
                    for i in 0 ..< feeds.count {
                        if ((feeds[i]["id"] as? String)! == pidStr) {
                            index = i
                        }
                    }
                    if (index > -1) {
                        var feed = feeds[index] as! Dictionary<String, String>
                        feed["read"] = "1"
                        feeds[index] = feed
                    }
                    self.collectionView?.reloadData()
                }
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.delegate = self
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }

            // Analytics
            Mixpanel.sharedInstance().track("Post opened")
        }
    }
    
    //DetailViewController Delegate
    func detailViewControllerFavChanged(detailViewController: DetailViewController) {
        reloadData()
    }
}

