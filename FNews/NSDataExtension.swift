//
//  NSDataExtension.swift
//  FNews
//
//  Created by Gabor Rusznyak on 26/12/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation

extension NSData {

    func saveToCache(urlString: String) {
        let fileName = urlString.replace("://", with: "_").replace("/", with: "_")
        let cachePath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0]
        let path = "\(cachePath)/\(fileName)"
        self.writeToFile(path, atomically: true)
    }
    
    func saveToDocuments(urlString: String) {
        let fileName = urlString.replace("://", with: "_").replace("/", with: "_")
        let documentsPath = NSFileManager.documentPath
        let path = "\(documentsPath)/\(fileName)"
        self.writeToFile(path, atomically: true)
    }
    
}
