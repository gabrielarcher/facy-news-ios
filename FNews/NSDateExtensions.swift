//
//  DateExtensions.swift
//  Csajok_a_motoron
//
//  Created by Rusznyák Gábor on 18/10/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation

extension NSDate {

    convenience init(dateString: String) {
        let dateStr = dateString.substringTo(19).replace("T", with: " ") + " " + dateString.substringFrom(19)
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateStr)
        self.init(timeInterval: 0, sinceDate: d!)
    }
    
    convenience init(twDateString: String) {
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let dateStr = twDateString.substringTo(19).replace("T", with: " ") + " " + twDateString.substringFrom(4)
        for i in 0 ..< months.count {
            if (dateStr.contains(months[i])) {
                
            }
        }
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateStr)
        self.init(timeInterval: 0, sinceDate: d!)
    }
    
    convenience init(timestamp: Int) {
        let interval = NSTimeInterval(timestamp)
        self.init(timeIntervalSince1970: interval)
    }
    
    convenience init(year: Int, month: Int, day: Int) {
        self.init(year: year, month: month, day: day, hour: 0, minute: 0, second: 0)
    }
    
    convenience init(year: Int, month: Int, day: Int, hour: Int, minute: Int) {
        self.init(year: year, month: month, day: day, hour: hour, minute: minute, second: 0)
    }
    
    convenience init(year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int) {
        let components = NSDateComponents()
        components.year = year
        components.month = month
        components.day = day
        components.hour = hour
        components.minute = minute
        components.second = second
        let calendar = NSCalendar.currentCalendar()
        self.init(timeInterval: 0, sinceDate:calendar.dateFromComponents(components)!)
    }
    
    var year: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Year, fromDate: self)
            return components.year
        }
    }
    
    var month: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Month, fromDate: self)
            return components.month
        }
    }
    
    var day: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Day, fromDate: self)
            return components.day
        }
    }
    
    var hour: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Hour, fromDate: self)
            return components.hour
        }
    }
    
    var minute: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Minute, fromDate: self)
            return components.minute
        }
    }
    
    var second: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Second, fromDate: self)
            return components.second
        }
    }
    
    var weekday: Int {
        get {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Weekday, fromDate: self)
            return components.weekday
        }
    }
    
    var timestamp: Int {
        return Int(self.timeIntervalSince1970)
    }
    
    func shortDate() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        return dateFormatter.stringFromDate(self)
    }
    
    func shortTime() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        return dateFormatter.stringFromDate(self)
    }
    
    func shortDateTime() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        dateFormatter.timeStyle = .ShortStyle
        return dateFormatter.stringFromDate(self)
    }
    
    func longDate() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .LongStyle
        return dateFormatter.stringFromDate(self)
    }
    
    func longTime() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .LongStyle
        return dateFormatter.stringFromDate(self)
    }
    
    func toLongString() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .LongStyle
        dateFormatter.timeStyle = .LongStyle
        
        return dateFormatter.stringFromDate(self)
    }

}