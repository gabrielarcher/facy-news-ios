//
//  NSFileManagerExtension.swift
//  Suna-Swift
//
//  Created by Gabor Rusznyak on 18/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation

extension NSFileManager {
    class var documentPath: String {
        get {
            return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] 
        }
    }
    
    class var cachePath: String {
        get {
            return NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] 
        }
    }
    
    class func clearCache() {
        let directory = NSFileManager.cachePath
        let fileManager = NSFileManager.defaultManager()
        do {
            let fileArray = try fileManager.contentsOfDirectoryAtPath(directory)
            for fileName in fileArray {
                do {
                    try fileManager.removeItemAtPath("\(directory)/\(fileName)")
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }

    }

}