//
//  PagesCell.swift
//  FNews
//
//  Created by Gabor Rusznyak on 27/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

class PagesCell: UITableViewCell {
    
    @IBOutlet var logoView: UIImageView?
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var categoryLabel: UILabel?
    @IBOutlet var switcher: UISwitch?

    var logoLink = ""
    
}
