//
//  PagesViewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 27/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit
import StoreKit
import Mixpanel

class PagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIAlertViewDelegate {
    
    @IBOutlet var searchBar: UISearchBar?
    @IBOutlet var tableView: UITableView?
    @IBOutlet var segmentedControl: UISegmentedControl?

    var pages = Array<AnyObject>()
    var order = "page_name"
    var refreshView = UIRefreshControl()
    let facebookLoader = FacebookLoader()
    var isRestore = false
    var siteId = 1
    
    override func viewDidLoad() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(facebookPagesLoaded(_:)), name:"NotificationFacebookPagesLoaded", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(facebookLoadError(_:)), name:"NotificationFacebookLoadError", object: nil)
        
        if let orderStr = NSUserDefaults.standardUserDefaults().stringForKey("Order") {
            order = orderStr
            segmentedControl?.selectedSegmentIndex = order == "page_name" ? 0 : 1
        }
        
        //Pull to refresh
        refreshView.attributedTitle = NSAttributedString(string: String.localised("REFRESHING_PAGES"))
        refreshView.addTarget(self, action:#selector(refresh(_:)), forControlEvents: .ValueChanged)
        tableView?.addSubview(refreshView)
        tableView?.alwaysBounceVertical = true
        
        setCustomBackButton("BackBtn")
    }

    override func viewWillAppear(animated: Bool) {
        title = String.localised("PAGES")
        if (isPhone) {
            navigationController?.toolbar.hidden = true
        }
        segmentedControl?.setTitle(String.localised("A_Z"), forSegmentAtIndex: 0)
        segmentedControl?.setTitle(String.localised("TIME"), forSegmentAtIndex: 1)
        
        //Search Bar button
        for subView in searchBar!.subviews[0].subviews {
            if (subView.conformsToProtocol(UITextInputTraits)) {
                (subView as! UITextField).returnKeyType = .Done
                (subView as! UITextField).enablesReturnKeyAutomatically = false
            }
        }
        
        reloadData()
        
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        tableView?.reloadData()
        tableView?.layoutIfNeeded()
    }

    func refresh(sender: UIRefreshControl) {
        refreshView.attributedTitle = NSAttributedString(string: String.localised("REFRESHING"))
        refreshView.beginRefreshing()
        facebookLoader.loadPages()
    }

    
    func reloadData() {
        var whereStr = ""
        if (!searchBar!.text!.isEmpty) {
            whereStr = " and page_name like '%\(searchBar!.text!)%'"
        }
        
        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
            self.pages = SQLiteHelper.instance.query("select * from pages where page_source=\(self.siteId) \(whereStr) order by \(self.order)")
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView!.reloadData()
            })
        })
    }
    
    func dropboxSync(notification: NSNotification) {
        reloadData()
    }
    
    //MARK: - IBActions
    
    @IBAction func setPageOrder(sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 0) {
            order = "page_name"
        } else {
            order = "time desc"
        }
        NSUserDefaults.standardUserDefaults().setObject(order, forKey: "Order")
        NSUserDefaults.standardUserDefaults().synchronize()
        reloadData()
    }
    
    @IBAction func switchPage(sender: UISwitch) {

        // Analytics
        Mixpanel.sharedInstance().track("Page switched")
        
        //Check active pages
        var activePages = 0
        for page in pages {
            if let active = page["active"] as? String {
                if let act: Int = Int(active) {
                    activePages += act
                }
            }
        }
        
        let page = pages[sender.tag] as! Dictionary<String, AnyObject>
        
        if let pageId = page["page_id"] as! String? {
            dispatch_async(Commons.instance.sqlQueue, { () -> Void in
                SQLiteHelper.instance.execute("update pages set active = \(sender.on ? 1 : 0) where page_id = \(pageId)")
                self.pages = SQLiteHelper.instance.query("select * from pages order by \(self.order)")
            })
        }
    }
    
    func back(sender: UIButton) {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK: - Facebook Loader Delegate
    
    func facebookPagesLoaded(notification: NSNotification) {
        if (refreshView.refreshing) {
            refreshView.endRefreshing()
        }
        refreshView.attributedTitle = NSAttributedString(string: String.localised("PULL_TO_REFRESH"))
        reloadData()
    }
    
    func facebookLoadError(notification: NSNotification) {
        if (refreshView.refreshing) {
            refreshView.endRefreshing()
        }
        refreshView.attributedTitle = NSAttributedString(string: String.localised("PULL_TO_REFRESH"))
    }

    //MARK: - Search Bar delegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //MARK: - Table View delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pages.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: PagesCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PagesCell
        
        let page = pages[indexPath.item] as! Dictionary<String, AnyObject>
        cell.nameLabel?.text = page["page_name"] as? String
        cell.categoryLabel?.text = page["category"] as? String
        if let active = page["active"] as? String {
            cell.switcher?.on = (active == "1")
        }
        cell.switcher?.tag = indexPath.item
        
        cell.width = tableView.width
        cell.switcher?.x = cell.width - 57
        cell.nameLabel?.width = cell.width - 125
        
        //Download logo
        let pageId = page["page_id"] as! String?
        let urlStr2 = "https://graph.facebook.com/\(pageId!)/picture?type=small"
        cell.logoLink = urlStr2
        if let image = UIImage.imageFromCache(urlStr2) {
            cell.logoView?.image = image
        } else {
            cell.logoView?.image = nil
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                if let imageData = NSData(contentsOfURL: NSURL(string: urlStr2)!) {
                    imageData.saveToCache(urlStr2)
                    let image=UIImage(data: imageData)
                    if (image != nil) {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            if (cell.logoLink == urlStr2) {
                                cell.logoView!.image = image!
                            }
                        })
                    }
                }
            })
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let page = pages[indexPath.row] as! Dictionary<String, AnyObject>
        if let active = page["active"] as? String {
            if (active == "1") {
                if let pagePostsController = storyboard?.instantiateViewControllerWithIdentifier("PagePosts") as? MasterViewController {
                    pagePostsController.pageId = page["page_id"] as! String
                    pagePostsController.pageName = page["page_name"] as! String
                    navigationController?.pushViewController(pagePostsController, animated: true)
                }
            }
        }
    }
    
    
}
