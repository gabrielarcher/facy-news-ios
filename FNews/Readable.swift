//
//  Readable.swift
//  FNews
//
//  Created by Gabor Rusznyak on 30/03/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import UIKit

class Readable: NSObject {
   

    class var instance : Readable {
        struct Static {
            static let instance : Readable = Readable()
        }
        return Static.instance
    }
    
    func getContentFromWeb(URLString: String, completionHandler: (success: Bool, response: String) -> Void) {
        let request = NSURLRequest(URL: NSURL(string: "http://www.readability.com/m?url=\(URLString.urlEncode())")!)
        print(request.URL!.description)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response, data, error) -> Void in
            if (error == nil) {
                let responseText = NSString(data: data!, encoding: NSUTF8StringEncoding)
                let text = self.getContentFromHTML(responseText! as String)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completionHandler(success: true, response: text)
                })
                
            } else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completionHandler(success: false, response: "")
                })
            }
            
        }

    }

    func getContentFromHTML(HTMLString: String) -> String {
        
        var result = HTMLString
        
        //Remove new lines
        result = result.regReplace("[\r\n\t]", with: "")
        
        //Get ARTICLE tag
        result = result.regReplace("<article.*?>", with: "[article]")
        result = result.regReplace("</article.*?>", with: "[/article]")
        
        let from = result.indexOf("[article]") + 9
        let to = result.indexOf("[/article]", startIndex: from + 1)
        result = result.subStringFrom(from, length: to - from - 1)
        
        //Replace <br>, <p> to \n
        result = result.regReplace("<br.*?>", with: "\n")
        result = result.regReplace("<p.*?>", with: "\n")
        result = result.regReplace("</p.*?>", with: "\n")
        result = result.regReplace("<h.*?>", with: "\n")
        result = result.regReplace("</h.*?>", with: "\n")
        
        //Remove all tags
        result = result.removeAllTags()
        
        //Replace too much white spaces
        while (result.contains("  ")) {
            result = result.replace("  ", with: " ")
        }
        
        //Replace Escapes
        result = result.htmlDecode()
        
        return result
    }
    

}
