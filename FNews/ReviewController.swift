//
//  ReviewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 01/02/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import UIKit

protocol ReviewControllerDelegate {
    func reviewControllerRated(reviewController: ReviewController, goToStore: Bool, sendEmail: Bool)
}

class ReviewController: UIViewController, UIAlertViewDelegate {
    
    @IBOutlet var backView: UIView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var textLabel: UILabel?
    @IBOutlet var noButton: UIButton?
    @IBOutlet var stars: Array<UIButton>?
    
    var delegate: ReviewControllerDelegate?
    var isStore = false
    
    override func viewWillAppear(animated: Bool) {
        titleLabel?.text = String.localised("REVIEW_TITLE")
        textLabel?.text = String.localised("REVIEW_TEXT")
        noButton?.setTitle(String.localised("NOT_NOW"), forState: .Normal)
        backView?.alpha = 0
        UIView.animateWithDuration(NSTimeInterval(0.5), animations: { () -> Void in
            self.backView!.alpha = 1
        })
        for i in 0 ..< 5 {
            stars![i].selected = false
        }

    }
    
    func canShow() -> Bool {
        if (NSUserDefaults.standardUserDefaults().boolForKey("ReviewNotAnymore")) {
            return false
        }
        let lastTime = NSUserDefaults.standardUserDefaults().integerForKey("ReviewLastTime")
        let lastOpen = NSUserDefaults.standardUserDefaults().integerForKey("ReviewLastOpen")
        if (lastTime != 0) {
            if (NSDate().timestamp > lastTime + 864000) {
                if (lastOpen >= 9) {
                    NSUserDefaults.standardUserDefaults().setInteger(NSDate().timestamp, forKey: "ReviewLastTime")
                    NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "ReviewLastOpen")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    return true
                }
            }
        } else {
            NSUserDefaults.standardUserDefaults().setInteger(NSDate().timestamp, forKey: "ReviewLastTime")
        }
        
        NSUserDefaults.standardUserDefaults().setInteger(lastOpen + 1, forKey: "ReviewLastOpen")
        NSUserDefaults.standardUserDefaults().synchronize()

        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func setStar(sender: UIButton) {
        for i in 0 ..< sender.tag {
            stars![i].selected = true
        }
        
        if (sender.tag > 3) {
            isStore = true
            UIAlertView(title: String.localised("THANKS"), message: String.localised("REVIEW_STORE"), delegate: self, cancelButtonTitle: String.localised("NOT_ANYMORE"), otherButtonTitles: String.localised("YES"), String.localised("NOT_NOW")).show()
        } else {
            isStore = false
            UIAlertView(title: String.localised("THANKS"), message: String.localised("REVIEW_EMAIL"), delegate: self, cancelButtonTitle: String.localised("NOT_ANYMORE"), otherButtonTitles: String.localised("YES"), String.localised("NOT_NOW")).show()
        }
    }
    
    @IBAction func notNow(sender: UIButton) {
        self.delegate?.reviewControllerRated(self, goToStore: false, sendEmail: false)
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 1:
            //Yes
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "ReviewNotAnymore")
            if (isStore) {
                self.delegate?.reviewControllerRated(self, goToStore: true, sendEmail: false)
            } else {
                self.delegate?.reviewControllerRated(self, goToStore: false, sendEmail: true)
            }
        case 2:
            //Not Now
            self.delegate?.reviewControllerRated(self, goToStore: false, sendEmail: false)
        default:
            //Not anymore
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "ReviewNotAnymore")
            self.delegate?.reviewControllerRated(self, goToStore: false, sendEmail: false)
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}
