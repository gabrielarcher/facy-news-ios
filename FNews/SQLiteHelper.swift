//
//  SQLiteHelper.swift
//  Astrology
//
//  Created by Rusznyák Gábor on 29/10/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

class SQLiteHelper: NSObject {
    
    private var dbPath = ""
    private var database: COpaquePointer = nil
    private var opened = false

    class var instance: SQLiteHelper {
        struct Static {
            static let instance: SQLiteHelper = SQLiteHelper()
        }
        return Static.instance
    }
    

    func open(databaseName: String, inDocuments: Bool) {
        /*
        inDocuments says if you want to copy the database into the
        Documents folder (read/write) or leave it in resources (read only)
        */
        
        var resPath = ""
        if (inDocuments) {
            dbPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] 
            resPath = NSBundle.mainBundle().resourcePath! +  "/" + databaseName
        } else {
            dbPath = NSBundle.mainBundle().resourcePath!
        }
        dbPath += "/" + databaseName
        print(dbPath)
        
        if (!NSFileManager.defaultManager().fileExistsAtPath(dbPath)) {
            do {
                try NSFileManager.defaultManager().copyItemAtPath(resPath, toPath: dbPath)
            } catch _ {
            }
        }
        opened = (sqlite3_open(dbPath.cStringUsingEncoding(NSUTF8StringEncoding)!, &database) == SQLITE_OK)
        if (!opened) {
            print("Cannot open database!")
        }

    }
    
    func query(sqlStr: String) -> Array<Dictionary<String, String>> {
        var result: Array<Dictionary<String, String>> = []
        if (opened) {
            var stmt: COpaquePointer = nil
            let sql = sqlStr.cStringUsingEncoding(NSUTF8StringEncoding)
            if (sqlite3_prepare_v2(database, sql!, -1, &stmt, nil) == SQLITE_OK) {
                while(sqlite3_step(stmt) == SQLITE_ROW) {
                    var line = Dictionary<String, String>()
                    for i: Int32 in 0 ..< sqlite3_column_count(stmt) {
                        let buffer = UnsafePointer<Int8>(sqlite3_column_name(stmt, i))
                        let field = String.fromCString(buffer) as String!
                        let buffer2 = UnsafePointer<Int8>(sqlite3_column_text(stmt, i))
                        let value = String.fromCString(buffer2)
                        line[field] = value
                    }
                    result.append(line)
                }
            } else {
                print("SQL error: \(sqlite3_errmsg(database).debugDescription) \(sqlStr)!")
            }
            sqlite3_finalize(stmt)
        } else {
            print("Cannot open database!")
        }
        return result
    }
    
    func execute(sqlStr: String) -> Int {
        var result = 0
        if (opened) {
            var stmt: COpaquePointer = nil
            let sql = sqlStr.cStringUsingEncoding(NSUTF8StringEncoding)
            sqlite3_exec(database, "BEGIN EXCLUSIVE TRANSACTION", nil, nil, nil);
            if (sqlite3_prepare_v2(database, sql!, -1, &stmt, nil) == SQLITE_OK) {
                let status = sqlite3_step(stmt)
                if (status == SQLITE_DONE) {
                    result = 1;
                    if (sqlite3_last_insert_rowid(database) > 0) {
                        result = Int(sqlite3_last_insert_rowid(database))
                    }
                }
            } else {
                print("SQL error: \(sqlite3_errmsg(database).debugDescription) \(sqlStr)!")
            }
            if (sqlite3_exec(database, "COMMIT TRANSACTION", nil, nil, nil) != SQLITE_OK) {
                print("SQL Error: \(sqlite3_errmsg(database))");
            }
            sqlite3_finalize(stmt)
        } else {
            print("Cannot open database!")
            return result
        }
        return result
    }
    
}

