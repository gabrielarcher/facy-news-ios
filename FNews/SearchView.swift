//
//  SearchView.swift
//  FNews
//
//  Created by Gabriel Archer on 24/08/2016.
//  Copyright © 2016 swGuru Ltd. All rights reserved.
//

import UIKit

class SearchView: UICollectionReusableView {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    override func prepareForReuse() {
        searchBar.placeholder = NSLocalizedString("SEARCH", comment: "")
        for view: UIView in searchBar.subviews[0].subviews {
            if let textField = view as? UITextField {
                textField.enablesReturnKeyAutomatically = false
            }
        }
    }
}
