//
//  SettingsCell.swift
//  FNews
//
//  Created by Gabor Rusznyak on 28/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var switcher: UISwitch?
    @IBOutlet var backView: UIView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
