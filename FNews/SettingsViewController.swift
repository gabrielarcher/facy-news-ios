//
//  SettingsViewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 28/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit
import StoreKit

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var backButton: UIBarButtonItem?
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setCustomBackButton("BackBtn")
    }

    override func viewWillAppear(animated: Bool) {
        title = String.localised("SETTINGS")
        navigationController?.navigationBar.hidden = false
        navigationController?.toolbar.hidden = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Header"), forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        tableView?.reloadData()
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.tableView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    
    @IBAction func setDefaultValue(sender: UISwitch) {
        switch sender.tag {
        case 0:
            NSUserDefaults.standardUserDefaults().setBool(sender.on, forKey: "SyncWifiOnly")
        default:
            NSUserDefaults.standardUserDefaults().setBool(sender.on, forKey: "ShowReadPosts")
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 2
        default:
            return 2
        }
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRectMake(20, 0, 300, 60))
        label.font = UIFont.boldSystemFontOfSize(18)
        label.backgroundColor = UIColor(red: 222.0/255.0, green: 222.0/255.0, blue: 222.0/255.0, alpha: 1)
        switch section {
        case 0:
            label.text = "   " + String.localised("SYNC")
        case 1:
            label.text = "   " + String.localised("POSTS")
        default:
            label.text = "   " + String.localised("FEEDBACK")
        }
        
        return label
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SettingsCell

        cell.backView?.frame = cell.bounds
        cell.switcher?.hidden = false

        switch indexPath.section {
        case 0:
            if (indexPath.row == 0) {
                let interval = String.defaultValue(Commons.instance.intervals[Commons.instance.selectedInterval]["name"] as? String)
                cell.nameLabel?.text = String.localised("SYNC_INTERVAL") + ": \(interval)"
                cell.switcher?.hidden = true
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            } else if (indexPath.row == 1) {
                cell.nameLabel?.text = String.localised("WIFI_ONLY")
                cell.switcher?.on = NSUserDefaults.standardUserDefaults().boolForKey("SyncWifiOnly")
                cell.switcher?.tag = 0
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        case 1:
            if (indexPath.row == 0) {
                cell.nameLabel?.text = String.localised("SHOW_READ")
                cell.switcher?.on = NSUserDefaults.standardUserDefaults().boolForKey("ShowReadPosts")
                cell.switcher?.tag = 2
                cell.accessoryType = UITableViewCellAccessoryType.None
            } else if (indexPath.row == 1) {
                let interval = String.defaultValue(Commons.instance.reserveIntervals[Commons.instance.reserveInterval]["name"] as? String)
                cell.nameLabel?.text = String.localised("RESERVE_POSTS") + ": \(interval)"
                cell.switcher?.hidden = true
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            }
        default:
            cell.switcher?.hidden = true
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            if (indexPath.row == 0) {
                cell.nameLabel?.text = String.localised("SEND_MESSAGE")
            } else {
                cell.nameLabel?.text = String.localised("RATE_WRITE")
            }
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == 0 && indexPath.row == 0) {
            if let intervalController = storyboard?.instantiateViewControllerWithIdentifier("Interval") as? IntervalViewController {
                intervalController.isReserve = false
                navigationController?.pushViewController(intervalController, animated: true)
            }
        } else if (indexPath.section == 1 && indexPath.row == 1) {
            if let intervalController = storyboard?.instantiateViewControllerWithIdentifier("Interval") as? IntervalViewController {
                intervalController.isReserve = true
                navigationController?.pushViewController(intervalController, animated: true)
            }
        } else if (indexPath.section == 2) {
            if (indexPath.row == 0) {
                //Send an email
                self.mainViewController?.writeEmail()
            } else {
                //Open Store window
                self.mainViewController?.openStore()
            }
        }
    }
    
}
