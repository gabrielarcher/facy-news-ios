//
//  StringExtension.swift
//  Suna
//
//  Created by Rusznyák Gábor on 15/10/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func urlEncode() -> String {
        return self.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
    }
    
    func urlDecode() -> String {
        return self.stringByRemovingPercentEncoding!
    }
    
    func split(separator: String) -> Array<String> {
        return self.componentsSeparatedByString(separator)
    }
    
    func replace(pattern: String, with: String) -> String {
        return self.stringByReplacingOccurrencesOfString(pattern, withString: with, options: NSStringCompareOptions.CaseInsensitiveSearch, range: self.range())
    }
    
    func regReplace(pattern: String, with: String) -> String {
        let range = NSMakeRange(0, self.characters.count)
        if let regex = try? NSRegularExpression(pattern: pattern, options: .CaseInsensitive) {
            let result = regex.stringByReplacingMatchesInString(self, options: [], range: range, withTemplate: with)
            return result
        }
        return self
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func range() -> Range<String.Index> {
        return Range(self.startIndex ..< self.startIndex.advancedBy(self.characters.count))
    }
    
    func contains(pattern: String) -> Bool {
        return self.rangeOfString(pattern) != nil
    }
    
    func substringFrom(index: Int) -> String {
        if (self.length >= index) {
            let strIndex: String.Index = self.startIndex.advancedBy(index)
            return self.substringFromIndex(strIndex)
        }
        return ""
    }
    
    func substringTo(index: Int) -> String {
        if (self.length >= index) {
            let strIndex: String.Index = self.startIndex.advancedBy(index)
            return self.substringToIndex(strIndex)
        }
        return self
    }
    
    func subStringFrom(from: Int, length: Int) -> String {
        if (length < 0) {
            return ""
        }
        var fromIndex = from
        if (fromIndex < 0) {
            fromIndex = 0
        }
        var toIndex = from + length
        if (toIndex < 0) {
            toIndex = 0
        }
        if (self.length >= fromIndex) {
            if (self.length >= toIndex) {
                let range = Range(self.startIndex.advancedBy(fromIndex) ..< self.startIndex.advancedBy(toIndex))
                return self.substringWithRange(range)
            } else {
                return self.substringFrom(fromIndex)
            }
        } else {
            if (self.length >= toIndex) {
                return self.substringTo(toIndex)
            } else {
                return ""
            }
        }
    }
    
    func isEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }
    
    func containsRegex(regExp: String) -> Bool {
        let regexpTest = NSPredicate(format:"SELF MATCHES %@", regExp)
        return regexpTest.evaluateWithObject(self)
    }
    
    func width(font: UIFont) -> CGFloat {
        let attributes = [NSFontAttributeName: font]
        let attributedText = NSAttributedString(string: self, attributes: attributes)
        let rect = attributedText.boundingRectWithSize(CGSizeMake(CGFloat.max, 30), options: .UsesLineFragmentOrigin, context: nil)
        return rect.size.width
    }
    
    func height(font: UIFont, width: CGFloat) -> CGFloat {
        let attributes = [NSFontAttributeName: font]
        let attributedText = NSAttributedString(string: self, attributes: attributes)
        let rect = attributedText.boundingRectWithSize(CGSizeMake(width, CGFloat.max), options: .UsesLineFragmentOrigin, context: nil)
        return rect.size.height
    }
    
    static func defaultValue(string: String?) -> String {
        if (string == nil) {
            return ""
        }
        return string!
    }
    
    func indexOf(target: String) -> Int {
        let range = self.rangeOfString(target)
        if let range = range {
            return self.startIndex.distanceTo(range.startIndex)
        } else {
            return -1
        }
    }
    
    func indexOf(target: String, startIndex: Int) -> Int {
        let startRange = self.startIndex.advancedBy(startIndex)
        let range = self.rangeOfString(target, options: NSStringCompareOptions.LiteralSearch, range: Range<String.Index>(startRange ..< self.endIndex))
        if let range = range {
            return self.startIndex.distanceTo(range.startIndex)
        } else {
            return -1
        }
    }
    
    func lastIndexOf(target: String) -> Int {
        var index = -1
        var stepIndex = self.indexOf(target)
        while stepIndex > -1
        {
            index = stepIndex
            if stepIndex + target.length < self.length {
                stepIndex = indexOf(target, startIndex: stepIndex + target.length)
            } else {
                stepIndex = -1
            }
        }
        return index
    }
    
    static func localised(key: String) -> String {
        return NSLocalizedString(key, tableName: nil, bundle: NSBundle.mainBundle(), value: key, comment: "")
    }
    
    func toFixLength(length: Int) -> String {
        var str = self
        for _ in 0 ..< length {
            str += " "
        }
        return str.substringTo(length);
    }
    
    func removeAccent() -> String {
        let data = self.replace("’", with: "'").dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)
        if (data != nil) {
            return NSString(data: data!, encoding: NSASCIIStringEncoding)! as String
        } else {
            return ""
        }
    }
    
    func removeTag(tag: String) -> String {
        
        var result = self
        
        result = result.regReplace("<\(tag).*?>", with: "[\(tag)]")
        result = result.regReplace("</\(tag).*?>", with: "[/\(tag)]")
        
        while (result.contains("[\(tag)]")) {
            let from = result.indexOf("[\(tag)]")
            let to = result.indexOf("[/\(tag)]", startIndex: from + 1) + tag.length + 3
            result = result.substringTo(from) + result.substringFrom(to)
        }

        return result
    }
    
    func removeTagWithKeyword(keyword: String) -> String {
        
        var result = ""
        var beginIndex = 0
        var matches = [AnyObject]()
        if let regex = try? NSRegularExpression(pattern: "<.*?>", options: .CaseInsensitive) {
            
            matches = regex.matchesInString(self, options: [], range: NSMakeRange(0, self.length))
            print("Matches: \(matches.count)")
            if (matches.count > 0) {
                for match in matches {
                    let tag = self.subStringFrom(match.range.location, length: match.range.length)
                    if tag.contains(keyword) {
                        //println(tag)
                        let pos = tag.indexOf(" ", startIndex: 0)
                        let tagName = tag.subStringFrom(1, length: pos - 1)
                        let addStr = self.subStringFrom(beginIndex, length: match.range.location - beginIndex)
                        //println("Begin: \(beginIndex)")
                        result += addStr
                        if (tag.contains("/>")) {
                            //self closed tag
                            let bIndex = match.range.location + tag.length
                            if (bIndex > beginIndex) {
                                beginIndex = bIndex
                            }
                            //println("Closed New: \(beginIndex)")
                        } else {
                            //Search closing tag
                            let bIndex = self.indexOf("</\(tagName)>", startIndex: match.range.location + 1) + tagName.length + 3
                            if (bIndex > beginIndex) {
                                beginIndex = bIndex
                            } else {
                                beginIndex = match.range.location + tag.length
                            }
                            //println("Unclosed New: \(beginIndex)")
                        }
                    }
                }
            }
            
        }
        
        return result
    }
    
    func removeAllButBody() -> String {

        var result = self
        let tag = "body"
        
        result = result.regReplace("<\(tag).*?>", with: "[\(tag)]")
        result = result.regReplace("</\(tag).*?>", with: "[/\(tag)]")

        let from = result.indexOf("[\(tag)]") + tag.length + 2
        let to = result.indexOf("[/\(tag)]", startIndex: from + 1)
        result = result.subStringFrom(from, length: to - from)

        return result
    }
    
    func removeAllTags() -> String {
        return self.regReplace("<.*?>", with: "")
    }
    
        func base64Encode() -> String {
        if let plainData = (self as NSString).dataUsingEncoding(NSUTF8StringEncoding) {
            return plainData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        }
        return ""
    }
    
    func base64Decode() -> String {
        if let decodedData = NSData(base64EncodedString: self, options:.IgnoreUnknownCharacters) {
            return NSString(data: decodedData, encoding: NSUTF8StringEncoding) as? String ?? ""
        }
        return ""
    }
}
