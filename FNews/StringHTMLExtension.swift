//
//  StringHTMLExtension.swift
//  ReadabilityTest
//
//  Created by Gabor Rusznyak on 30/03/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import Foundation

struct HTMLEscapeMap {
    var escapeSequence = ""
    var unichar: UniChar = 0
}

extension String {

    private func asciiHTMLEscapeMap() -> Array<HTMLEscapeMap> {
        
        var result = Array<HTMLEscapeMap>()
        
        // A.2.2. Special characters
        result.append(HTMLEscapeMap(escapeSequence: "&quot;", unichar: 34))
        result.append(HTMLEscapeMap(escapeSequence: "&amp;", unichar: 38))
        result.append(HTMLEscapeMap(escapeSequence: "&apos;", unichar: 39))
        result.append(HTMLEscapeMap(escapeSequence: "&lt;", unichar: 60))
        result.append(HTMLEscapeMap(escapeSequence: "&gt;", unichar: 62))
        
        // A.2.1. Latin-1 characters
        result.append(HTMLEscapeMap(escapeSequence: "&nbsp;", unichar: 160))
        result.append(HTMLEscapeMap(escapeSequence: "&iexcl;", unichar: 161))
        result.append(HTMLEscapeMap(escapeSequence: "&cent;", unichar: 162))
        result.append(HTMLEscapeMap(escapeSequence: "&pound;", unichar: 163))
        result.append(HTMLEscapeMap(escapeSequence: "&curren;", unichar: 164))
        result.append(HTMLEscapeMap(escapeSequence: "&yen;", unichar: 165))
        result.append(HTMLEscapeMap(escapeSequence: "&brvbar;", unichar: 166))
        result.append(HTMLEscapeMap(escapeSequence: "&sect;", unichar: 167))
        result.append(HTMLEscapeMap(escapeSequence: "&uml;", unichar: 168))
        result.append(HTMLEscapeMap(escapeSequence: "&copy;", unichar: 169))
        result.append(HTMLEscapeMap(escapeSequence: "&ordf;", unichar: 170))
        result.append(HTMLEscapeMap(escapeSequence: "&laquo;", unichar: 171))
        result.append(HTMLEscapeMap(escapeSequence: "&not;", unichar: 172))
        result.append(HTMLEscapeMap(escapeSequence: "&shy;", unichar: 173))
        result.append(HTMLEscapeMap(escapeSequence: "&reg;", unichar: 174))
        result.append(HTMLEscapeMap(escapeSequence: "&macr;", unichar: 175))
        result.append(HTMLEscapeMap(escapeSequence: "&deg;", unichar: 176))
        result.append(HTMLEscapeMap(escapeSequence: "&plusmn;", unichar: 177))
        result.append(HTMLEscapeMap(escapeSequence: "&sup2;", unichar: 178))
        result.append(HTMLEscapeMap(escapeSequence: "&sup3;", unichar: 179))
        result.append(HTMLEscapeMap(escapeSequence: "&acute;", unichar: 180))
        result.append(HTMLEscapeMap(escapeSequence: "&micro;", unichar: 181))
        result.append(HTMLEscapeMap(escapeSequence: "&para;", unichar: 182))
        result.append(HTMLEscapeMap(escapeSequence: "&middot;", unichar: 183))
        result.append(HTMLEscapeMap(escapeSequence: "&cedil;", unichar: 184))
        result.append(HTMLEscapeMap(escapeSequence: "&sup1;", unichar: 185))
        result.append(HTMLEscapeMap(escapeSequence: "&ordm;", unichar: 186))
        result.append(HTMLEscapeMap(escapeSequence: "&raquo;", unichar: 187))
        result.append(HTMLEscapeMap(escapeSequence: "&frac14;", unichar: 188))
        result.append(HTMLEscapeMap(escapeSequence: "&frac12;", unichar: 189))
        result.append(HTMLEscapeMap(escapeSequence: "&frac34;", unichar: 190))
        result.append(HTMLEscapeMap(escapeSequence: "&iquest;", unichar: 191))
        result.append(HTMLEscapeMap(escapeSequence: "&Agrave;", unichar: 192))
        result.append(HTMLEscapeMap(escapeSequence: "&Aacute;", unichar: 193))
        result.append(HTMLEscapeMap(escapeSequence: "&Acirc;", unichar: 194))
        result.append(HTMLEscapeMap(escapeSequence: "&Atilde;", unichar: 195))
        result.append(HTMLEscapeMap(escapeSequence: "&Auml;", unichar: 196))
        result.append(HTMLEscapeMap(escapeSequence: "&Aring;", unichar: 197))
        result.append(HTMLEscapeMap(escapeSequence: "&AElig;", unichar: 198))
        result.append(HTMLEscapeMap(escapeSequence: "&Ccedil;", unichar: 199))
        result.append(HTMLEscapeMap(escapeSequence: "&Egrave;", unichar: 200))
        result.append(HTMLEscapeMap(escapeSequence: "&Eacute;", unichar: 201))
        result.append(HTMLEscapeMap(escapeSequence: "&Ecirc;", unichar: 202))
        result.append(HTMLEscapeMap(escapeSequence: "&Euml;", unichar: 203))
        result.append(HTMLEscapeMap(escapeSequence: "&Igrave;", unichar: 204))
        result.append(HTMLEscapeMap(escapeSequence: "&Iacute;", unichar: 205))
        result.append(HTMLEscapeMap(escapeSequence: "&Icirc;", unichar: 206))
        result.append(HTMLEscapeMap(escapeSequence: "&Iuml;", unichar: 207))
        result.append(HTMLEscapeMap(escapeSequence: "&ETH;", unichar: 208))
        result.append(HTMLEscapeMap(escapeSequence: "&Ntilde;", unichar: 209))
        result.append(HTMLEscapeMap(escapeSequence: "&Ograve;", unichar: 210))
        result.append(HTMLEscapeMap(escapeSequence: "&Oacute;", unichar: 211))
        result.append(HTMLEscapeMap(escapeSequence: "&Ocirc;", unichar: 212))
        result.append(HTMLEscapeMap(escapeSequence: "&Otilde;", unichar: 213))
        result.append(HTMLEscapeMap(escapeSequence: "&Ouml;", unichar: 214))
        result.append(HTMLEscapeMap(escapeSequence: "&times;", unichar: 215))
        result.append(HTMLEscapeMap(escapeSequence: "&Oslash;", unichar: 216))
        result.append(HTMLEscapeMap(escapeSequence: "&Ugrave;", unichar: 217))
        result.append(HTMLEscapeMap(escapeSequence: "&Uacute;", unichar: 218))
        result.append(HTMLEscapeMap(escapeSequence: "&Ucirc;", unichar: 219))
        result.append(HTMLEscapeMap(escapeSequence: "&Uuml;", unichar: 220))
        result.append(HTMLEscapeMap(escapeSequence: "&Yacute;", unichar: 221))
        result.append(HTMLEscapeMap(escapeSequence: "&THORN;", unichar: 222))
        result.append(HTMLEscapeMap(escapeSequence: "&szlig;", unichar: 223))
        result.append(HTMLEscapeMap(escapeSequence: "&agrave;", unichar: 224))
        result.append(HTMLEscapeMap(escapeSequence: "&aacute;", unichar: 225))
        result.append(HTMLEscapeMap(escapeSequence: "&acirc;", unichar: 226))
        result.append(HTMLEscapeMap(escapeSequence: "&atilde;", unichar: 227))
        result.append(HTMLEscapeMap(escapeSequence: "&auml;", unichar: 228))
        result.append(HTMLEscapeMap(escapeSequence: "&aring;", unichar: 229))
        result.append(HTMLEscapeMap(escapeSequence: "&aelig;", unichar: 230))
        result.append(HTMLEscapeMap(escapeSequence: "&ccedil;", unichar: 231))
        result.append(HTMLEscapeMap(escapeSequence: "&egrave;", unichar: 232))
        result.append(HTMLEscapeMap(escapeSequence: "&eacute;", unichar: 233))
        result.append(HTMLEscapeMap(escapeSequence: "&ecirc;", unichar: 234))
        result.append(HTMLEscapeMap(escapeSequence: "&euml;", unichar: 235))
        result.append(HTMLEscapeMap(escapeSequence: "&igrave;", unichar: 236))
        result.append(HTMLEscapeMap(escapeSequence: "&iacute;", unichar: 237))
        result.append(HTMLEscapeMap(escapeSequence: "&icirc;", unichar: 238))
        result.append(HTMLEscapeMap(escapeSequence: "&iuml;", unichar: 239))
        result.append(HTMLEscapeMap(escapeSequence: "&eth;", unichar: 240))
        result.append(HTMLEscapeMap(escapeSequence: "&ntilde;", unichar: 241))
        result.append(HTMLEscapeMap(escapeSequence: "&ograve;", unichar: 242))
        result.append(HTMLEscapeMap(escapeSequence: "&oacute;", unichar: 243))
        result.append(HTMLEscapeMap(escapeSequence: "&ocirc;", unichar: 244))
        result.append(HTMLEscapeMap(escapeSequence: "&otilde;", unichar: 245))
        result.append(HTMLEscapeMap(escapeSequence: "&ouml;", unichar: 246))
        result.append(HTMLEscapeMap(escapeSequence: "&divide;", unichar: 247))
        result.append(HTMLEscapeMap(escapeSequence: "&oslash;", unichar: 248))
        result.append(HTMLEscapeMap(escapeSequence: "&ugrave;", unichar: 249))
        result.append(HTMLEscapeMap(escapeSequence: "&uacute;", unichar: 250))
        result.append(HTMLEscapeMap(escapeSequence: "&ucirc;", unichar: 251))
        result.append(HTMLEscapeMap(escapeSequence: "&uuml;", unichar: 252))
        result.append(HTMLEscapeMap(escapeSequence: "&yacute;", unichar: 253))
        result.append(HTMLEscapeMap(escapeSequence: "&thorn;", unichar: 254))
        result.append(HTMLEscapeMap(escapeSequence: "&yuml;", unichar: 255))
        
        // A.2.2. Special characters cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&OElig;", unichar: 338))
        result.append(HTMLEscapeMap(escapeSequence: "&oelig;", unichar: 339))
        result.append(HTMLEscapeMap(escapeSequence: "&Scaron;", unichar: 352))
        result.append(HTMLEscapeMap(escapeSequence: "&scaron;", unichar: 353))
        result.append(HTMLEscapeMap(escapeSequence: "&Yuml;", unichar: 376))
        
        // A.2.3. Symbols
        result.append(HTMLEscapeMap(escapeSequence: "&fnof;", unichar: 402))
        
        // A.2.2. Special characters cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&circ;", unichar: 710))
        result.append(HTMLEscapeMap(escapeSequence: "&tilde;", unichar: 732))
        
        // A.2.3. Symbols cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&Alpha;", unichar: 913))
        result.append(HTMLEscapeMap(escapeSequence: "&Beta;", unichar: 914))
        result.append(HTMLEscapeMap(escapeSequence: "&Gamma;", unichar: 915))
        result.append(HTMLEscapeMap(escapeSequence: "&Delta;", unichar: 916))
        result.append(HTMLEscapeMap(escapeSequence: "&Epsilon;", unichar: 917))
        result.append(HTMLEscapeMap(escapeSequence: "&Zeta;", unichar: 918))
        result.append(HTMLEscapeMap(escapeSequence: "&Eta;", unichar: 919))
        result.append(HTMLEscapeMap(escapeSequence: "&Theta;", unichar: 920))
        result.append(HTMLEscapeMap(escapeSequence: "&Iota;", unichar: 921))
        result.append(HTMLEscapeMap(escapeSequence: "&Kappa;", unichar: 922))
        result.append(HTMLEscapeMap(escapeSequence: "&Lambda;", unichar: 923))
        result.append(HTMLEscapeMap(escapeSequence: "&Mu;", unichar: 924))
        result.append(HTMLEscapeMap(escapeSequence: "&Nu;", unichar: 925))
        result.append(HTMLEscapeMap(escapeSequence: "&Xi;", unichar: 926))
        result.append(HTMLEscapeMap(escapeSequence: "&Omicron;", unichar: 927))
        result.append(HTMLEscapeMap(escapeSequence: "&Pi;", unichar: 928))
        result.append(HTMLEscapeMap(escapeSequence: "&Rho;", unichar: 929))
        result.append(HTMLEscapeMap(escapeSequence: "&Sigma;", unichar: 931))
        result.append(HTMLEscapeMap(escapeSequence: "&Tau;", unichar: 932))
        result.append(HTMLEscapeMap(escapeSequence: "&Upsilon;", unichar: 933))
        result.append(HTMLEscapeMap(escapeSequence: "&Phi;", unichar: 934))
        result.append(HTMLEscapeMap(escapeSequence: "&Chi;", unichar: 935))
        result.append(HTMLEscapeMap(escapeSequence: "&Psi;", unichar: 936))
        result.append(HTMLEscapeMap(escapeSequence: "&Omega;", unichar: 937))
        result.append(HTMLEscapeMap(escapeSequence: "&alpha;", unichar: 945))
        result.append(HTMLEscapeMap(escapeSequence: "&beta;", unichar: 946))
        result.append(HTMLEscapeMap(escapeSequence: "&gamma;", unichar: 947))
        result.append(HTMLEscapeMap(escapeSequence: "&delta;", unichar: 948))
        result.append(HTMLEscapeMap(escapeSequence: "&epsilon;", unichar: 949))
        result.append(HTMLEscapeMap(escapeSequence: "&zeta;", unichar: 950))
        result.append(HTMLEscapeMap(escapeSequence: "&eta;", unichar: 951))
        result.append(HTMLEscapeMap(escapeSequence: "&theta;", unichar: 952))
        result.append(HTMLEscapeMap(escapeSequence: "&iota;", unichar: 953))
        result.append(HTMLEscapeMap(escapeSequence: "&kappa;", unichar: 954))
        result.append(HTMLEscapeMap(escapeSequence: "&lambda;", unichar: 955))
        result.append(HTMLEscapeMap(escapeSequence: "&mu;", unichar: 956))
        result.append(HTMLEscapeMap(escapeSequence: "&nu;", unichar: 957))
        result.append(HTMLEscapeMap(escapeSequence: "&xi;", unichar: 958))
        result.append(HTMLEscapeMap(escapeSequence: "&omicron;", unichar: 959))
        result.append(HTMLEscapeMap(escapeSequence: "&pi;", unichar: 960))
        result.append(HTMLEscapeMap(escapeSequence: "&rho;", unichar: 961))
        result.append(HTMLEscapeMap(escapeSequence: "&sigmaf;", unichar: 962))
        result.append(HTMLEscapeMap(escapeSequence: "&sigma;", unichar: 963))
        result.append(HTMLEscapeMap(escapeSequence: "&tau;", unichar: 964))
        result.append(HTMLEscapeMap(escapeSequence: "&upsilon;", unichar: 965))
        result.append(HTMLEscapeMap(escapeSequence: "&phi;", unichar: 966))
        result.append(HTMLEscapeMap(escapeSequence: "&chi;", unichar: 967))
        result.append(HTMLEscapeMap(escapeSequence: "&psi;", unichar: 968))
        result.append(HTMLEscapeMap(escapeSequence: "&omega;", unichar: 969))
        result.append(HTMLEscapeMap(escapeSequence: "&thetasym;", unichar: 977))
        result.append(HTMLEscapeMap(escapeSequence: "&upsih;", unichar: 978))
        result.append(HTMLEscapeMap(escapeSequence: "&piv;", unichar: 982))
        
        // A.2.2. Special characters cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&ensp;", unichar: 8194))
        result.append(HTMLEscapeMap(escapeSequence: "&emsp;", unichar: 8195))
        result.append(HTMLEscapeMap(escapeSequence: "&thinsp;", unichar: 8201))
        result.append(HTMLEscapeMap(escapeSequence: "&zwnj;", unichar: 8204))
        result.append(HTMLEscapeMap(escapeSequence: "&zwj;", unichar: 8205))
        result.append(HTMLEscapeMap(escapeSequence: "&lrm;", unichar: 8206))
        result.append(HTMLEscapeMap(escapeSequence: "&rlm;", unichar: 8207))
        result.append(HTMLEscapeMap(escapeSequence: "&ndash;", unichar: 8211))
        result.append(HTMLEscapeMap(escapeSequence: "&mdash;", unichar: 8212))
        result.append(HTMLEscapeMap(escapeSequence: "&lsquo;", unichar: 8216))
        result.append(HTMLEscapeMap(escapeSequence: "&rsquo;", unichar: 8217))
        result.append(HTMLEscapeMap(escapeSequence: "&sbquo;", unichar: 8218))
        result.append(HTMLEscapeMap(escapeSequence: "&ldquo;", unichar: 8220))
        result.append(HTMLEscapeMap(escapeSequence: "&rdquo;", unichar: 8221))
        result.append(HTMLEscapeMap(escapeSequence: "&bdquo;", unichar: 8222))
        result.append(HTMLEscapeMap(escapeSequence: "&dagger;", unichar: 8224))
        result.append(HTMLEscapeMap(escapeSequence: "&Dagger;", unichar: 8225))
        // A.2.3. Symbols cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&bull;", unichar: 8226))
        result.append(HTMLEscapeMap(escapeSequence: "&hellip;", unichar: 8230))
        
        // A.2.2. Special characters cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&permil;", unichar: 8240))
        
        // A.2.3. Symbols cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&prime;", unichar: 8242))
        result.append(HTMLEscapeMap(escapeSequence: "&Prime;", unichar: 8243))
        
        // A.2.2. Special characters cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&lsaquo;", unichar: 8249))
        result.append(HTMLEscapeMap(escapeSequence: "&rsaquo;", unichar: 8250))
        
        // A.2.3. Symbols cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&oline;", unichar: 8254))
        result.append(HTMLEscapeMap(escapeSequence: "&frasl;", unichar: 8260))
        
        // A.2.2. Special characters cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&euro;", unichar: 8364))
        
        // A.2.3. Symbols cont'd
        result.append(HTMLEscapeMap(escapeSequence: "&image;", unichar: 8465))
        result.append(HTMLEscapeMap(escapeSequence: "&weierp;", unichar: 8472))
        result.append(HTMLEscapeMap(escapeSequence: "&real;", unichar: 8476))
        result.append(HTMLEscapeMap(escapeSequence: "&trade;", unichar: 8482))
        result.append(HTMLEscapeMap(escapeSequence: "&alefsym;", unichar: 8501))
        result.append(HTMLEscapeMap(escapeSequence: "&larr;", unichar: 8592))
        result.append(HTMLEscapeMap(escapeSequence: "&uarr;", unichar: 8593))
        result.append(HTMLEscapeMap(escapeSequence: "&rarr;", unichar: 8594))
        result.append(HTMLEscapeMap(escapeSequence: "&darr;", unichar: 8595))
        result.append(HTMLEscapeMap(escapeSequence: "&harr;", unichar: 8596))
        result.append(HTMLEscapeMap(escapeSequence: "&crarr;", unichar: 8629))
        result.append(HTMLEscapeMap(escapeSequence: "&lArr;", unichar: 8656))
        result.append(HTMLEscapeMap(escapeSequence: "&uArr;", unichar: 8657))
        result.append(HTMLEscapeMap(escapeSequence: "&rArr;", unichar: 8658))
        result.append(HTMLEscapeMap(escapeSequence: "&dArr;", unichar: 8659))
        result.append(HTMLEscapeMap(escapeSequence: "&hArr;", unichar: 8660))
        result.append(HTMLEscapeMap(escapeSequence: "&forall;", unichar: 8704))
        result.append(HTMLEscapeMap(escapeSequence: "&part;", unichar: 8706))
        result.append(HTMLEscapeMap(escapeSequence: "&exist;", unichar: 8707))
        result.append(HTMLEscapeMap(escapeSequence: "&empty;", unichar: 8709))
        result.append(HTMLEscapeMap(escapeSequence: "&nabla;", unichar: 8711))
        result.append(HTMLEscapeMap(escapeSequence: "&isin;", unichar: 8712))
        result.append(HTMLEscapeMap(escapeSequence: "&notin;", unichar: 8713))
        result.append(HTMLEscapeMap(escapeSequence: "&ni;", unichar: 8715))
        result.append(HTMLEscapeMap(escapeSequence: "&prod;", unichar: 8719))
        result.append(HTMLEscapeMap(escapeSequence: "&sum;", unichar: 8721))
        result.append(HTMLEscapeMap(escapeSequence: "&minus;", unichar: 8722))
        result.append(HTMLEscapeMap(escapeSequence: "&lowast;", unichar: 8727))
        result.append(HTMLEscapeMap(escapeSequence: "&radic;", unichar: 8730))
        result.append(HTMLEscapeMap(escapeSequence: "&prop;", unichar: 8733))
        result.append(HTMLEscapeMap(escapeSequence: "&infin;", unichar: 8734))
        result.append(HTMLEscapeMap(escapeSequence: "&ang;", unichar: 8736))
        result.append(HTMLEscapeMap(escapeSequence: "&and;", unichar: 8743))
        result.append(HTMLEscapeMap(escapeSequence: "&or;", unichar: 8744))
        result.append(HTMLEscapeMap(escapeSequence: "&cap;", unichar: 8745)) 
        result.append(HTMLEscapeMap(escapeSequence: "&cup;", unichar: 8746)) 
        result.append(HTMLEscapeMap(escapeSequence: "&int;", unichar: 8747)) 
        result.append(HTMLEscapeMap(escapeSequence: "&there4;", unichar: 8756)) 
        result.append(HTMLEscapeMap(escapeSequence: "&sim;", unichar: 8764)) 
        result.append(HTMLEscapeMap(escapeSequence: "&cong;", unichar: 8773)) 
        result.append(HTMLEscapeMap(escapeSequence: "&asymp;", unichar: 8776)) 
        result.append(HTMLEscapeMap(escapeSequence: "&ne;", unichar: 8800)) 
        result.append(HTMLEscapeMap(escapeSequence: "&equiv;", unichar: 8801)) 
        result.append(HTMLEscapeMap(escapeSequence: "&le;", unichar: 8804)) 
        result.append(HTMLEscapeMap(escapeSequence: "&ge;", unichar: 8805)) 
        result.append(HTMLEscapeMap(escapeSequence: "&sub;", unichar: 8834)) 
        result.append(HTMLEscapeMap(escapeSequence: "&sup;", unichar: 8835)) 
        result.append(HTMLEscapeMap(escapeSequence: "&nsub;", unichar: 8836)) 
        result.append(HTMLEscapeMap(escapeSequence: "&sube;", unichar: 8838)) 
        result.append(HTMLEscapeMap(escapeSequence: "&supe;", unichar: 8839)) 
        result.append(HTMLEscapeMap(escapeSequence: "&oplus;", unichar: 8853)) 
        result.append(HTMLEscapeMap(escapeSequence: "&otimes;", unichar: 8855)) 
        result.append(HTMLEscapeMap(escapeSequence: "&perp;", unichar: 8869)) 
        result.append(HTMLEscapeMap(escapeSequence: "&sdot;", unichar: 8901)) 
        result.append(HTMLEscapeMap(escapeSequence: "&lceil;", unichar: 8968)) 
        result.append(HTMLEscapeMap(escapeSequence: "&rceil;", unichar: 8969)) 
        result.append(HTMLEscapeMap(escapeSequence: "&lfloor;", unichar: 8970)) 
        result.append(HTMLEscapeMap(escapeSequence: "&rfloor;", unichar: 8971)) 
        result.append(HTMLEscapeMap(escapeSequence: "&lang;", unichar: 9001)) 
        result.append(HTMLEscapeMap(escapeSequence: "&rang;", unichar: 9002)) 
        result.append(HTMLEscapeMap(escapeSequence: "&loz;", unichar: 9674)) 
        result.append(HTMLEscapeMap(escapeSequence: "&spades;", unichar: 9824)) 
        result.append(HTMLEscapeMap(escapeSequence: "&clubs;", unichar: 9827)) 
        result.append(HTMLEscapeMap(escapeSequence: "&hearts;", unichar: 9829)) 
        result.append(HTMLEscapeMap(escapeSequence: "&diams;", unichar: 9830))

        return result
    }

    private func unicodeHTMLEscapeMap() -> Array<HTMLEscapeMap> {

        var result = Array<HTMLEscapeMap>()

        // C0 Controls and Basic Latin
        result.append(HTMLEscapeMap(escapeSequence: "&quot;", unichar: 34))
        result.append(HTMLEscapeMap(escapeSequence: "&amp;", unichar: 38))
        result.append(HTMLEscapeMap(escapeSequence: "&apos;", unichar: 39))
        result.append(HTMLEscapeMap(escapeSequence: "&lt;", unichar: 60))
        result.append(HTMLEscapeMap(escapeSequence: "&gt;", unichar: 62))
        
        // Latin Extended-A
        result.append(HTMLEscapeMap(escapeSequence: "&OElig;", unichar: 338))
        result.append(HTMLEscapeMap(escapeSequence: "&oelig;", unichar: 339))
        result.append(HTMLEscapeMap(escapeSequence: "&Scaron;", unichar: 352))
        result.append(HTMLEscapeMap(escapeSequence: "&scaron;", unichar: 353))
        result.append(HTMLEscapeMap(escapeSequence: "&Yuml;", unichar: 376))
        
        // Spacing Modifier Letters
        result.append(HTMLEscapeMap(escapeSequence: "&circ;", unichar: 710))
        result.append(HTMLEscapeMap(escapeSequence: "&tilde;", unichar: 732))
        
        // General Punctuation
        result.append(HTMLEscapeMap(escapeSequence: "&ensp;", unichar: 8194))
        result.append(HTMLEscapeMap(escapeSequence: "&emsp;", unichar: 8195))
        result.append(HTMLEscapeMap(escapeSequence: "&thinsp;", unichar: 8201))
        result.append(HTMLEscapeMap(escapeSequence: "&zwnj;", unichar: 8204))
        result.append(HTMLEscapeMap(escapeSequence: "&zwj;", unichar: 8205))
        result.append(HTMLEscapeMap(escapeSequence: "&lrm;", unichar: 8206))
        result.append(HTMLEscapeMap(escapeSequence: "&rlm;", unichar: 8207))
        result.append(HTMLEscapeMap(escapeSequence: "&ndash;", unichar: 8211))
        result.append(HTMLEscapeMap(escapeSequence: "&mdash;", unichar: 8212))
        result.append(HTMLEscapeMap(escapeSequence: "&lsquo;", unichar: 8216))
        result.append(HTMLEscapeMap(escapeSequence: "&rsquo;", unichar: 8217))
        result.append(HTMLEscapeMap(escapeSequence: "&sbquo;", unichar: 8218))
        result.append(HTMLEscapeMap(escapeSequence: "&ldquo;", unichar: 8220))
        result.append(HTMLEscapeMap(escapeSequence: "&rdquo;", unichar: 8221))
        result.append(HTMLEscapeMap(escapeSequence: "&bdquo;", unichar: 8222))
        result.append(HTMLEscapeMap(escapeSequence: "&dagger;", unichar: 8224))
        result.append(HTMLEscapeMap(escapeSequence: "&Dagger;", unichar: 8225))
        result.append(HTMLEscapeMap(escapeSequence: "&permil;", unichar: 8240))
        result.append(HTMLEscapeMap(escapeSequence: "&lsaquo;", unichar: 8249))
        result.append(HTMLEscapeMap(escapeSequence: "&rsaquo;", unichar: 8250))
        result.append(HTMLEscapeMap(escapeSequence: "&euro;", unichar: 8364))
        
        return result
    }

    func htmlDecode() -> String {
        var finalString = self
        let regexp = "&.*?;"
        var matches = [AnyObject]()
        
        let regex = try! NSRegularExpression(pattern: regexp, options: .CaseInsensitive)
        matches = regex.matchesInString(finalString, options: [], range: NSMakeRange(0, finalString.length))
        
        var count = 0
        while matches.count > 0 && count < 30 {
            let escapeString = finalString.subStringFrom(matches[0].range.location, length: matches[0].range.length)
            print(escapeString)
            let length = escapeString.length
            if length > 3 && length < 11 {
                // a squence must be longer than 3 (&lt;) and less than 11 (&thetasym;)
                if escapeString.subStringFrom(1, length: 1) == "#" {
                    if escapeString.subStringFrom(2, length: 1).lowercaseString == "x" {
                        // Hex escape squences &#xa3;
                        let hexSequence = escapeString.subStringFrom(3, length: length - 4)
                        let scanner = NSScanner(string: hexSequence)
                        var value: UInt32 = 0
                        if scanner.scanHexInt(&value) {
                            if scanner.scanLocation == (length - 4) {
                                let character = Character(UnicodeScalar(value))
                                if value < 256 {
                                    finalString = finalString.replace(escapeString, with: "\(character)")
                                } else {
                                    finalString = finalString.replace(escapeString, with: "")
                                }
                            }
                        }
                    } else {
                        // Decimal Sequences &#123;
                        let decSequence = escapeString.subStringFrom(2, length: length - 3)
                        let scanner = NSScanner(string: decSequence)
                        var value: Int32 = 0
                        if scanner.scanInt(&value) {
                            if scanner.scanLocation == (length - 3) {
                                let character = Character(UnicodeScalar(Int(value)))
                                finalString = finalString.replace(escapeString, with: "\(character)")
                            }
                            
                        }
                    }
                } else {
                    // "standard" sequences
                    for asciiHTMLEscape in asciiHTMLEscapeMap() {
                        if (asciiHTMLEscape.escapeSequence == escapeString) {
                            let character = Character(UnicodeScalar(asciiHTMLEscape.unichar))
                            finalString = finalString.replace(escapeString, with: "\(character)")
                        }
                    }
                }
            }
            matches = regex.matchesInString(finalString, options: [], range: NSMakeRange(0, finalString.length))
            count += 1
        }
        
        return finalString.regReplace(regexp, with: "")
    }
}