//
//  TutorialController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 21/02/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import UIKit

class TutorialController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView?
    @IBOutlet var circles: Array<UIImageView>?
    @IBOutlet var pagerView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        for i in 0 ..< 6 {
            var type = "1"
            if (isPhone) {
                type = self.view.height == 480 ? "4" : "6"
            }
            let imageView = UIImageView(image: UIImage(named: "Tutor\(type)\(i)"))
            if (isPhone) {
                imageView.frame = CGRectMake(self.view.width * CGFloat(i), 0, self.view.width, self.view.height)
            } else if UIScreen.mainScreen().bounds.width > 1024 {
                imageView.frame = CGRectMake(1366 * CGFloat(i), 0, 1366, 1024)
            } else {
                imageView.frame = CGRectMake(1024 * CGFloat(i), 0, 1024, 768)
            }
            imageView.contentMode = .ScaleAspectFill
            scrollView?.addSubview(imageView)
        }
        if (isPhone) {
            scrollView?.contentSize = CGSizeMake(self.view.width * 6, self.view.height)
            if (self.view.height == 480) {
                pagerView?.backgroundColor = UIColor(netHex: 0x5e85e4)
            }
        } else if UIScreen.mainScreen().bounds.width > 1024 {
            scrollView?.contentSize = CGSizeMake(1366 * 6, 1024)
        } else {
            scrollView?.contentSize = CGSizeMake(1024 * 6, 768)
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pos = Int(scrollView.contentOffset.x / scrollView.width)
        for i in 0 ..< 6 {
            circles![i].image = UIImage(named: i == pos ? "FullCircle" : "Circle")
        }
        if (scrollView.contentOffset.x == self.view.width * CGFloat(5)) {
            self.presentingViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })
        }
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        if (isPhone) {
            return UIInterfaceOrientation.Portrait
        } else {
            return UIInterfaceOrientation.LandscapeRight
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
