//
//  TwitterLoader.swift
//  FNews
//
//  Created by Gabor Rusznyak on 08/02/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import UIKit

class TwitterLoader: NSObject {
   
    class var instance : TwitterLoader {
        struct Static {
            static let instance : TwitterLoader = TwitterLoader()
        }
        return Static.instance
    }
    
    
    func getTimeline(completionHandler: (success: Bool, posts: Int) -> Void) {
        TwitterHelper.instance.login { (success) -> Void in
            if (success) {
                var newPosts = 0
                TwitterHelper.instance.getData("statuses/home_timeline.json",
                    parameters: ["count": 2, "exclude_replies": true, "contributor_details": true],
                    completionHandler: { (response) -> Void in
                        if (response != nil) {
                            for tweet in response as [Dictionary<String, AnyObject>] {
                                if let user = tweet["user"] as? Dictionary<String, AnyObject> {
                                    
                                    //Get User
                                    let pageId = user["id_str"] as String
                                    let category = user["location"] as String
                                    var name = user["screen_name"] as String
                                    name = name.replace("\"", with: "'")
                                    let userImage = user["profile_image_url"] as String
                                    let createTime = user["created_at"] as String?
                                    var createDate = NSDate()
                                    if (createTime != nil) {
                                        createDate = NSDate(dateString: createTime!)
                                    }
                                    
                                    //Get Tweet
                                    let post_id = tweet["id_str"] as String
                                    var picture = ""
                                    var link = ""
                                    if let entities = tweet["entities"] as? Dictionary<String, AnyObject> {
                                        if let media = entities["media"] as? Array<AnyObject> {
                                            if media.count > 0 {
                                                if (media[0]["media_url"] != nil) {
                                                    picture = media[0]["media_url"] as String
                                                }
                                            }
                                        }
                                        if let urls = entities["urls"] as? Array<AnyObject> {
                                            if urls.count > 0 {
                                                if (urls[0]["expanded_url"] != nil) {
                                                    link = urls[0]["expanded_url"] as String
                                                }
                                            }
                                            
                                        }
                                    }
                                    let caption = tweet["text"] as String
                                    let time = tweet["created_at"] as String?
                                    let type = "link"
                                    var date = NSDate()
                                    if (time != nil) {
                                        date = NSDate(dateString: time!)
                                    }
                                    
                                    if (!link.isEmpty) {
                                        
                                        dispatch_async(Commons.instance.sqlQueue, { () -> Void in
/*
                                            var active = true
                                            let result = SQLiteHelper.instance.query("select active from pages where page_id=\(pageId) and page_source = 2")
                                            if (result.count == 0) {
                                                SQLiteHelper.instance.execute("insert into pages (page_id, category, page_name, active, page_source, time) values (\"\(pageId)\", \"\(category)\", \"\(name)\", 1, 2, \(createDate.timestamp))")
                                            } else {
                                                active = (result[0]["active"] == "1")
                                            }
                                            
                                            //Save the tweet
                                            if (active) {
                                                SQLiteHelper.instance.execute("insert into feeds (post_id, page, time, caption, author, " +
                                                    "link, name, picture, feed_type, read, fav) " +
                                                    "values (\"\(post_id)\", \"\(pageId)\", \"\(date.timestamp)\", \"\(caption)\", " +
                                                    "\"\(name)\", \"\(link)\", \"\(name)\", \"\(picture)\", \"\(type)\", 0, 0)")
                                                newPosts++
                                            }
*/                                            
                                            
                                        });

                                    }
                                }
                            }
                            println("stop")
                            completionHandler(success: true, posts: newPosts)
                        } else {
                            completionHandler(success: false, posts: 0)
                        }
                })
            } else {
                completionHandler(success: false, posts: 0)
            }
        }
    }
}
