//
//  UIImageExtension.swift
//  Suna
//
//  Created by Gabor Rusznyak on 11/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

extension UIImage {
    
    class func imageFromCache(urlString: String) -> UIImage? {
        let fileName = urlString.replace("://", with: "_").replace("/", with: "_")
        let cachePath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] 
        let path = "\(cachePath)/\(fileName)"
        if (NSFileManager.defaultManager().fileExistsAtPath(path)) {
            if let data = NSData(contentsOfFile: path) {
                return UIImage(data: data)
            }
        }
        return nil
    }
    
    class func imageFromDocuments(urlString: String) -> UIImage? {
        let fileName = urlString.replace("://", with: "_").replace("/", with: "_")
        let documentsPath = NSFileManager.documentPath
        let path = "\(documentsPath)/\(fileName)"
        if (NSFileManager.defaultManager().fileExistsAtPath(path)) {
            if let data = NSData(contentsOfFile: path) {
                return UIImage(data: data)
            }
        }
        return nil
    }
    
    func makeCircle(size: CGFloat, circleSize: CGFloat) -> UIImage {
        return makeCircle(size, circleSize: circleSize, topMargin: 0)
    }
    
    
    func makeCircle(size: CGFloat, circleSize: CGFloat, topMargin: CGFloat) -> UIImage {
        var image: UIImage?
        let margin = size - circleSize
        
        //Making mask
        UIGraphicsBeginImageContext(CGSizeMake(size * 2, size * 2));
        let maskContext = UIGraphicsGetCurrentContext()
        CGContextSetLineCap(maskContext, CGLineCap.Round)
        CGContextSetLineWidth(maskContext, 1.0)
        CGContextSetRGBFillColor(maskContext, 1.0, 1.0, 1.0, 1.0)
        CGContextFillRect(maskContext, CGRectMake(0, 0, size * 2, size * 2))
        CGContextSetRGBFillColor(maskContext, 0.0, 0.0, 0.0, 1.0)
        CGContextFillEllipseInRect(maskContext, CGRectMake(margin, margin + (topMargin * 2), circleSize * 2, circleSize * 2))
        let maskImage = CGBitmapContextCreateImage(maskContext)
        UIGraphicsEndImageContext();
        
        //Resize original image
        UIGraphicsBeginImageContext(CGSizeMake(size * 2, size * 2));
        self.makeSquare().drawInRect(CGRectMake(margin, margin + (topMargin * 2), circleSize * 2, circleSize * 2))
        let resizedImage = CGBitmapContextCreateImage(UIGraphicsGetCurrentContext())
        UIGraphicsEndImageContext();

        //Masking image
        let mask = CGImageMaskCreate(CGImageGetWidth(maskImage),
            CGImageGetHeight(maskImage),
            CGImageGetBitsPerComponent(maskImage),
            CGImageGetBitsPerPixel(maskImage),
            CGImageGetBytesPerRow(maskImage),
            CGImageGetDataProvider(maskImage), nil, true)
        let maskedImage = CGImageCreateWithMask(resizedImage, mask)
        image = UIImage(CGImage: maskedImage!)
        return image!
    }
    
    func makeSquare() -> UIImage {
        
        var pictSize: CGFloat = self.size.width
        var rect = CGRectMake(0, self.scale * (self.size.height - pictSize) / 2, self.scale * self.size.width, self.scale * self.size.height)
        if (self.size.height < self.size.width) {
            pictSize = self.size.height
            rect = CGRectMake(self.scale * (self.size.width - pictSize) / 2, 0, self.scale * self.size.width, self.scale * self.size.height)
        }

        let cgImage = CGImageCreateWithImageInRect(self.CGImage, rect)
        return UIImage(CGImage: cgImage!)
        
    }
        
    func rotate() -> UIImage {
        let imgRef = self.CGImage;
        
        let width = CGFloat(CGImageGetWidth(imgRef));
        let height = CGFloat(CGImageGetHeight(imgRef));
        
        
        var transform = CGAffineTransformIdentity;
        var bounds = CGRectMake(0, 0, width, height);
        let scaleRatio: CGFloat = 1;
        let imageSize = CGSizeMake(CGFloat(CGImageGetWidth(imgRef)), CGFloat(CGImageGetHeight(imgRef)));
        var boundHeight: CGFloat = 0;
        let orient = self.imageOrientation;
        
        switch(orient) {
            
        case UIImageOrientation.UpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientation.Down: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI));
            break;
            
        case UIImageOrientation.DownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientation.LeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * CGFloat(M_PI) / 2.0);
            break;
            
        case UIImageOrientation.Left: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * CGFloat(M_PI) / 2.0);
            break;
            
        case UIImageOrientation.RightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI) / 2.0);
            break;
            
        case UIImageOrientation.Right: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI) / 2.0);
            break;
            
        default: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
            
        }
        
        UIGraphicsBeginImageContext(bounds.size);
        let context = UIGraphicsGetCurrentContext();
        
        if (orient == UIImageOrientation.Right || orient == UIImageOrientation.Left) {
            CGContextScaleCTM(context, -scaleRatio, scaleRatio);
            CGContextTranslateCTM(context, -height, 0);
        }
        else {
            CGContextScaleCTM(context, scaleRatio, -scaleRatio);
            CGContextTranslateCTM(context, 0, -height);
        }
        
        CGContextConcatCTM(context, transform);
        
        CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
        let imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return imageCopy;
    }
}