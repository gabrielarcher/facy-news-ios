//
//  UIViewControllerExtension.swift
//  Suna
//
//  Created by Rusznyák Gábor on 14/10/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    var mainViewController: ViewController? {
        if let viewController = UIApplication.sharedApplication().windows[0].rootViewController as? ViewController {
            return viewController
        } else if let navController = UIApplication.sharedApplication().windows[0].rootViewController as? UINavigationController {
            return navController.childViewControllers[0] as? ViewController
        } else {
            return nil
        }
    }
    
    func alert(message: String) {
        UIAlertView(title: String.localised("WARNING"), message: message, delegate: nil, cancelButtonTitle: "OK").show()
    }
    
    func alert(title: String, message: String) {
        UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK").show()
    }
    
    var isPhone: Bool {
        return UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone
    }
    
    var isPortrait: Bool {
        let orientation = UIApplication.sharedApplication().statusBarOrientation
        return UIInterfaceOrientationIsPortrait(orientation)
    }
    
    func setCustomBackButton(imageName: String) {
        let backButton = UIButton(frame: CGRectMake(0, 0, 40, 40))
        backButton.setImage(UIImage(named: imageName), forState: .Normal)
        let barBackButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: Selector("back:"), forControlEvents: .TouchUpInside)
        self.navigationItem.leftBarButtonItem = barBackButtonItem
        self.navigationItem.hidesBackButton = true
    }
    

    
}

