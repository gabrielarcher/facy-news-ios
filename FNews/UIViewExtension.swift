//
//  UIViewExtension.swift
//  Suna
//
//  Created by Rusznyák Gábor on 14/10/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit

extension UIView {
    
    var x: CGFloat {
        get {
            return self.frame.origin.x
        }
        set(x) {
            self.frame = CGRectMake(x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
        }
    }

    var y: CGFloat {
        get {
            return self.frame.origin.y
        }
        set(y) {
            self.frame = CGRectMake(self.frame.origin.x, y, self.frame.size.width, self.frame.size.height)
        }
    }

    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set(width) {
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, width, self.frame.size.height)
        }
    }

    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set(height) {
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height)
        }
    }

    func flipFromLeft(target: UIView) {
        self.animation(target, transition: UIViewAnimationTransition.FlipFromLeft);
    }
    
    func flipFromRight(target: UIView) {
        self.animation(target, transition: UIViewAnimationTransition.FlipFromRight);
    }
    
    func curlUp(target: UIView) {
        self.animation(target, transition: UIViewAnimationTransition.CurlUp);
    }
    
    func pushFromLeft(target: UIView) {
        self.pushAnimation(target, transition: kCATransitionFromLeft);
    }
    
    func pushFromRight(target: UIView) {
        self.pushAnimation(target, transition: kCATransitionFromRight);
    }
    
    private func animation(target: UIView, transition: UIViewAnimationTransition) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationTransition(transition, forView: self, cache: true)
        while (self.subviews.count>0) {
            self.subviews[0].removeFromSuperview()
        }
        self.addSubview(target)
        UIView.commitAnimations()
    }
    
    private func pushAnimation(target: UIView, transition: String) {
        self.addSubview(target)
        let animation = CATransition()
        animation.duration = 0.5
        animation.type = kCATransitionPush
        animation.subtype = transition
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        animation.delegate = self
        self.layer.addAnimation(animation, forKey: "slide")
    }
    
    public override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        while (self.subviews.count>1) {
            self.subviews[0].removeFromSuperview()
        }
    }
    
    func setDefaultFont(fontName: String) {
        
        //Set the font of all UI element
        for subView in subviews {
            
            if let label = subView as? UILabel {
                label.font = UIFont(name: fontName, size: label.font.pointSize)
            } else if let textField = subView as? UITextField {
                textField.font = UIFont(name: fontName, size: textField.font!.pointSize)
            } else if let button = subView as? UIButton {
                button.titleLabel?.font = UIFont(name: fontName, size: button.titleLabel!.font.pointSize)
            }
            
            if subView.subviews.count > 0 {
                subView.setDefaultFont(fontName)
            }
            
        }
        
    }
    

}

