//
//  ViewController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 17/11/2014.
//  Copyright (c) 2014 swGuru Ltd. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI
import Mixpanel

class ViewController: UIViewController, UISplitViewControllerDelegate, SKStoreProductViewControllerDelegate, ReviewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    @IBOutlet var containerView: UIView?
    @IBOutlet var loaderView: UIView?
    @IBOutlet var loadingLabel: UILabel?
    
    var masterViewController: MasterViewController?
    var navController: UINavigationController?
    var reviewController: ReviewController?

    override func viewDidLoad() {
        if let splitViewController = self.childViewControllers[0] as? UISplitViewController {
            navController = splitViewController.viewControllers[splitViewController.viewControllers.count-2] as? UINavigationController
            let detailsNavController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as? UINavigationController
            detailsNavController?.topViewController?.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            splitViewController.delegate = self
            if (!isPhone) {
                splitViewController.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
            }
            if let masterController = splitViewController.childViewControllers[0].childViewControllers[0] as? MasterViewController {
                masterViewController = masterController
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.hidden = true
        loaderView?.hidden = true
        
        loadingLabel?.text = String.localised("LOADING")
        
        //Tutorial
        let tutored = NSUserDefaults.standardUserDefaults().boolForKey("Tutored")
        if (!tutored) {
            if let tutorialController = storyboard?.instantiateViewControllerWithIdentifier("Tutorial") as? TutorialController {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "Tutored")
                NSUserDefaults.standardUserDefaults().synchronize()
                self.presentViewController(tutorialController, animated: false, completion: nil)
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        reviewController = storyboard?.instantiateViewControllerWithIdentifier("Review") as? ReviewController
        if (reviewController != nil) {
            reviewController!.delegate = self
            if (reviewController!.canShow()) {
                self.view.addSubview(reviewController!.view)
            }
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    func openStore() {
        //Open Store window
        let storeController = SKStoreProductViewController()
        storeController.delegate = self
        let parameters = [SKStoreProductParameterITunesItemIdentifier: 969023300]
        storeController.loadProductWithParameters(parameters, completionBlock: { (result, error) -> Void in
            self.presentViewController(storeController, animated: true, completion: nil)
            
            // Analytics
            Mixpanel.sharedInstance().track("Store opened")
        })
    }

    func writeEmail() {
        if !MFMailComposeViewController.canSendMail() {
            alert("There is no account set to Mail app!")
            return
        }
        let msgPicker = MFMailComposeViewController()
        msgPicker.mailComposeDelegate = self
        msgPicker.setToRecipients(["swGuru Ltd<info@swguru.com>"])
        msgPicker.setSubject("Facy News app")
        msgPicker.setMessageBody("", isHTML: true)
        self.presentViewController(msgPicker, animated: true, completion: nil)

    }
    
    //MARK: - ReviewController delegate
    
    func reviewControllerRated(reviewController: ReviewController, goToStore: Bool, sendEmail: Bool) {
        reviewController.view.removeFromSuperview()
        if (goToStore) {
            openStore()
        } else if (sendEmail) {
            writeEmail()
        }
    }
    
    // MARK: - Split view
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
        if let secondaryAsNavController = secondaryViewController as? UINavigationController {
            if let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController {
                if topAsDetailController.detailItem == nil {
                    // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
                    return true
                }
            }
        }
        return false
    }
    
    //MARK: - Store Kit delegate
    
    func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Mail composer delegate
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }

}
