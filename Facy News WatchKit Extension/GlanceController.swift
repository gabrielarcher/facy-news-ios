//
//  GlanceController.swift
//  Facy News WatchKit Extension
//
//  Created by Gabor Rusznyak on 12/03/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {
    
    @IBOutlet var numberLabel: WKInterfaceLabel?

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        WKInterfaceController.openParentApplication(["op": 3, "postId": "-1"], reply: { (result, error) -> Void in
            if (error == nil) {
                
                self.numberLabel!.setText(result["number"] as? String)
            }
        })
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
