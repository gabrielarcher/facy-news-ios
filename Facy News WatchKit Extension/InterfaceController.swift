//
//  InterfaceController.swift
//  Facy News WatchKit Extension
//
//  Created by Gabor Rusznyak on 12/03/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    @IBOutlet var imageView: WKInterfaceImage?
    @IBOutlet var titleLabel: WKInterfaceLabel?
    @IBOutlet var nameLabel: WKInterfaceLabel?
    @IBOutlet var dateLabel: WKInterfaceLabel?
    @IBOutlet var timeLabel: WKInterfaceLabel?
    @IBOutlet var group: WKInterfaceGroup?

    var postId = "0"

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        titleLabel?.setText("")
        nameLabel?.setText("")
        dateLabel?.setText("")
        timeLabel?.setText("")
        imageView?.setImage(nil)
        postId = "0"
        sendRequest(1)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func sendRequest(heading: Int) {
        WKInterfaceController.openParentApplication(["op": heading, "postId": postId], reply: { (result, error) -> Void in
            if (error == nil) {
                println(result)
                self.postId = result["postId"] as String
                self.titleLabel?.setText(result["name"] as? String)
                self.nameLabel?.setText(result["page"] as? String)
                self.dateLabel?.setText(result["date"] as? String)
                self.timeLabel?.setText(result["time"] as? String)
                if result["image"] != nil {
                    self.imageView?.setImage(UIImage(data: result["image"] as NSData))
                } else {
                    self.imageView?.setImage(UIImage(named: "Logo"))
                }
            }
        })
    }
    
    @IBAction func read() {
        self.pushControllerWithName("TextController", context: postId)
    }

    @IBAction func fav() {
        WKInterfaceController.openParentApplication(["op": 2, "postId": postId], reply: { (result, error) -> Void in
            if (error == nil) {
                println(result)
                self.pushControllerWithName("AlertController", context: nil)
            }
        })
    }
    
    @IBAction func cancel() {
        
    }
    
    @IBAction func prev() {
        sendRequest(0)
    }
    
    @IBAction func next() {
        sendRequest(1)
    }
    
}
