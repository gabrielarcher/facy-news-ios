//
//  TextController.swift
//  FNews
//
//  Created by Gabor Rusznyak on 04/04/2015.
//  Copyright (c) 2015 swGuru Ltd. All rights reserved.
//

import WatchKit
import Foundation


class TextController: WKInterfaceController {
    
    var controllers = Array<String>()
    @IBOutlet var table: WKInterfaceTable!
    @IBOutlet var titleLabel: WKInterfaceLabel!

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        titleLabel.setText("Waiting...\n\n\n\n\n\n")
        
        controllers.append("TextRow")
        table.setRowTypes(controllers)
        if let rowController = table.rowControllerAtIndex(0) as? TextRowController {
            rowController.textLabel?.setText("\n\n\n\n\n\n")
        }
        sendRequest(context as String)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func sendRequest(postId: String) {
        WKInterfaceController.openParentApplication(["op": 9, "postId": postId], reply: { (result, error) -> Void in
            if (error == nil) {
                println(result)
                self.titleLabel.setText(result["page"] as? String)
                if let rowController = self.table.rowControllerAtIndex(0) as? TextRowController {
                    println(result)
                    rowController.textLabel?.setText(result["text"] as? String)
                }
            } else {
                self.titleLabel.setText("Warning")
                if let rowController = self.table.rowControllerAtIndex(0) as? TextRowController {
                    println(result)
                    rowController.textLabel?.setText("An error occured in the communication...")
                }
            }
        })
    }

}
